/// <references="Cypress" />
import {Basic} from '../../support/pages/basics/base.page'
import {Ofertas} from '../../support/pages/ofertas/ofertas-criar.page'

describe('OFERTAS - Cadastrar oferta', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/promotion')
        Basic.selecionar_shopping_sna();
    })
    it('Deve cadastrar Oferta e fazer assertiva', () =>{
        Ofertas.assertiva_pagina();
        Ofertas.acessar_criar_ofertas();
        Ofertas.upar_imagem();
        Ofertas.preencher_campos();
        Ofertas.salvar_rascunho();
        Ofertas.assertiva_rascunho();
        Ofertas.voltar_para_lista();
        Ofertas.filtra_rascunho();
        Ofertas.editar_rascunho();
        Ofertas.publicar_oferta();
        Ofertas.assertiva_publicacao();
        Ofertas.voltar_para_lista();
    })
})