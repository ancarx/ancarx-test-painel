/// <references="Cypress" />
import {Basic} from '../../support/pages/basics/base.page'
import {Ofertas_F} from '../../support/pages/ofertas/ofertas-filtrar.page'
import {Ofertas_E} from '../../support/pages/ofertas/ofertas-editar.page'
import {Ofertas} from '../../support/pages/ofertas/ofertas-criar.page'
describe('OFERTAS - Editar ofertas', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/promotion')
        Basic.selecionar_shopping_sna();
    })
    it('Deve editar ofertas', () =>{
        Ofertas.assertiva_pagina();
        Ofertas_F.acessar_filtro();
        Ofertas_F.preencher_filtros();
        Ofertas_F.clicar_filtrar();
        Ofertas_E.acessar_edição_oferta();
        Ofertas_E.assrtv_pagina();
        Ofertas_E.editar_oferta();
        Ofertas_E.salvar_edicao();
        Ofertas_E.assrtv_modal();
        Ofertas_E.voltar_para_lista();
    })
})