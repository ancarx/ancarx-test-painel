/// <references="Cypress" />
import {Basic} from '../../support/pages/basics/base.page'
import {Ofertas_F} from '../../support/pages/ofertas/ofertas-filtrar.page'
import {Ofertas} from '../../support/pages/ofertas/ofertas-criar.page'

describe('OFERTAS - Filtra ofertas através de status', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/promotion')
        Basic.selecionar_shopping_sna();
    })
    it('Deve filtrar ofertas e fazer assertiva', () =>{
        Ofertas.assertiva_pagina();
        Ofertas_F.acessar_filtro();
        Ofertas_F.visualizar_modal();
        Ofertas_F.botão_limpar_existe();
        Ofertas_F.preencher_filtros();
        Ofertas_F.clicar_filtrar();
        Ofertas_F.faz_assertiva();
    })
})