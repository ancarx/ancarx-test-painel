/// <references = "Cypress" />
import { Basic } from "../../support/pages/basics/base.page";
import { NF } from "../../support/pages/notafiscal/notafiscal-avaliar.page";
import { NF_F } from "../../support/pages/notafiscal/notafiscal-filtrar.page"

describe('NOTA FISCAL - Filtrar notas fiscais através de status', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/receipt')
        Basic.selecionar_shopping_sna();
    })
    it('Deve filtrar as notas fiscais por status e fazer assertiva', () =>{
        NF.assertivas_pagina();
        NF_F.acessar_filtro();
        NF_F.visualizar_modal();
        NF_F.limpar_filtro_existe();
        NF_F.preencher_filtro_status();
        NF_F.clicar_filtrar();
        NF_F.faz_assertiva_status();
    })
    it('Deve filtrar as notas fiscais por shopping e fazer assertiva', () =>{
        NF.assertivas_pagina();
        NF_F.acessar_filtro();
        NF_F.visualizar_modal();
        NF_F.limpar_filtro_existe();
        NF_F.preencher_filtro_shopping();
        NF_F.clicar_filtrar();
        NF_F.faz_assertiva_shopping();
    })
})