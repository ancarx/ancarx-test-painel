/// <references = "Cypress" />
import { Basic } from "../../support/pages/basics/base.page";
import { NF } from "../../support/pages/notafiscal/notafiscal-avaliar.page";

describe('NOTA FISCAL - Avaliar e finalizar avaliação de nota', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/receipt')
        Basic.selecionar_shopping_sna();
    })
    it('Deve avaliar e aprovar nota', () =>{
        NF.assertivas_pagina();
        NF.acessar_avaliacao();
        NF.preencher_avaliacao('Aprovar');
    })
    it('Deve avaliar e finalizar avaliação da nota', () =>{
        NF.assertivas_pagina();
        NF.acessar_avaliacao();
        NF.preencher_avaliacao('Finalizar avaliação')
    })
    it('Deve avaliar e reprovar avaliação da nota', () =>{
        NF.assertivas_pagina();
        NF.acessar_avaliacao();
        NF.preencher_reprovacao('Reprovar')
    })
    it('Deve avaliar e adiar avaliação da nota', () =>{
        NF.assertivas_pagina();
        NF.acessar_avaliacao();
        NF.preencher_adiamento('Adiar')
    })
})