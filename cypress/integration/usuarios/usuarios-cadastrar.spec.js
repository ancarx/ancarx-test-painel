/// <references="Cypress" />
import {Basic} from '../../support/pages/basics/base.page'
import {Usuarios} from '../../support/pages/usuarios/usuarios-cadastrar.pages'

describe('USUÁRIOS - Cadastrar usuário', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/user')
        Basic.selecionar_shopping_sna();
    })
    it('Deve cadastrar um novo usuário, Operador de Backoffice', () =>{
        Usuarios.assertiva_pag_usuarios();
        Usuarios.adicionar_usuario();
        Usuarios.assertiva_modal();
        Usuarios.preencher_modal('Operador de Backoffice');
        Usuarios.fechar_dropdown();
        Usuarios.preenche_senha();
        Usuarios.criar_usuario();
    })
    it('Deve cadastrar um novo usuário, Lojista', () =>{
        Usuarios.assertiva_pag_usuarios();
        Usuarios.adicionar_usuario();
        Usuarios.assertiva_modal();
        Usuarios.preencher_modal_lojista('Vendedor (Loja)');
        Usuarios.fechar_dropdown();
        Usuarios.seleciona_loja();
        Usuarios.preenche_senha();
        Usuarios.criar_usuario();
    })
    describe('Deve falhar ao cadastrar um novo usuário, Operador de Backoffice', () =>{
        it('Deve tentar cadastrar senhas diferentes', () =>{
            Usuarios.assertiva_pag_usuarios();
            Usuarios.adicionar_usuario();
            Usuarios.assertiva_modal();
            Usuarios.preencher_modal('Operador de Backoffice');
            Usuarios.preenche_senha_diferente();
            Usuarios.criar_usuario();
            Usuarios.assertiva_senhas_diferentes();
        })
        it('Deve tentar cadastrar sem preencher senha', () => {
            Usuarios.assertiva_pag_usuarios();
            Usuarios.adicionar_usuario();
            Usuarios.assertiva_modal();
            Usuarios.preencher_modal('Operador de Backoffice');
            Usuarios.criar_usuario();
            Usuarios.assertiva_senha_obrigatoria();
        })        
    })
})