import {Basic} from '../../support/pages/basics/base.page'
import {Usuarios} from '../../support/pages/usuarios/usuarios-cadastrar.pages'
import { Usuarios_A } from '../../support/pages/usuarios/usuarios-alterar.pages';

describe('USUARIOS - Alterar usuário', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/user')
        Basic.selecionar_shopping_sna();
    })
    it('Deve acessar /usuários e editar o usuário "Cypress USER" (Operador de Backoffice)', () =>{
        Usuarios.assertiva_pag_usuarios();
        Usuarios_A.busca_usuario();
        Usuarios_A.editar_usuario();
        Usuarios_A.editar_dados();
        Usuarios_A.confirmar_alteracao();
        Usuarios_A.confirmacao_modal();
        Usuarios_A.assertiva_nome_editado();
    })
})