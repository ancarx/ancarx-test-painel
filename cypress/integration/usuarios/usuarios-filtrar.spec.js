/// <references="Cypress" />
import {Basic} from '../../support/pages/basics/base.page'
import {Usuarios} from '../../support/pages/usuarios/usuarios-cadastrar.pages'
import { Usuarios_F } from '../../support/pages/usuarios/usuarios-filtrar.pages'

describe('USUARIOS - Filtrar usuários por Shopping, Situação e perfil', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/user')
        Basic.selecionar_shopping_sna();
    })
    it('Deve filtrar usuários e fazer assertiva', () =>{
        Usuarios.assertiva_pag_usuarios();
        Usuarios_F.acessar_filtro();
        Usuarios_F.assertiva_modal();
        Usuarios_F.preencher_filtro();
        Usuarios_F.filtrar();
        Usuarios_F.assertiva_filtrados_por_situacao();
        Usuarios_F.assertiva_filtrados_por_perfil();
    })
})