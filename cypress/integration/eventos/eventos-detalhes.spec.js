/// <references="Cypress" />
import { Basic } from "../../support/pages/basics/base.page";
import { Eventos } from "../../support/pages/eventos/eventos-criar.page";
import { Eventos_D } from "../../support/pages/eventos/eventos-detalhes.page";

describe('EVENTOS - Listar os eventos do shopping e Visualizar os detalhes de um evento', ()=>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/event')
        Basic.selecionar_shopping_sna();
    })
    it('Deve listar os eventos do shopping', () =>{
        Eventos.assertiva_pag_eventos();
        Eventos_D.assertiva_eventos();
    })
    it('Deve visualizar os detalhes de um evento', function(){
        Eventos_D.pega_valores_e_compara_com_detalhe();
        Eventos_D.voltar_para_lista_de_eventos();
    })
})