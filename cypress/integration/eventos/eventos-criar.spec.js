/// <references="Cypress" />
import { Basic } from "../../support/pages/basics/base.page";
import { Eventos } from "../../support/pages/eventos/eventos-criar.page";

describe('EVENTOS - Cadastrar um novo evento', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/event')
        Basic.selecionar_shopping_sna();
    })
    it('Deve acessar eventos e cadastrar um novo evento', () =>{
        Eventos.assertiva_pag_eventos();
        Eventos.acessar_criar_evento();
        Eventos.upar_imagem();
        Eventos.preenche_campos();
        Eventos.acessa_modal_evento();
        Eventos.preenche_modal_com_registro();
        Eventos.assertiva_modal_evento();
        Eventos.acessa_modal_evento();
        Eventos.preenche_modal_sem_registro();
        Eventos.assertiva_modal_evento();
        Eventos.salvar_publicar_evento();
        Eventos.assrtv_cadastro_salvo();
        Eventos.voltar_para_lista();
    })
})