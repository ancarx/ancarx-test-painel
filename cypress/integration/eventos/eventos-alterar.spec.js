/// <references="Cypress" />
import { Basic } from "../../support/pages/basics/base.page";
import { Eventos } from "../../support/pages/eventos/eventos-criar.page"
import { Eventos_A } from "../../support/pages/eventos/eventos-alterar.pages"

describe('EVENTOS - Alterar um evento', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/event')
        Basic.selecionar_shopping_sna();
    })
    it('Deve editar um evento', () =>{
        Eventos.assertiva_pag_eventos();
        Eventos_A.filtrar_evento();
        Eventos_A.editar_evento();
        Eventos_A.editar_campos();
        Eventos_A.salvar_edicao();
        Eventos_A.assertiva_modal_edicao();
        Eventos_A.confirmar_edicao();
    })
    it('Deve desistir de editar um evento', () =>{
        Eventos.assertiva_pag_eventos();
        Eventos_A.filtrar_evento();
        Eventos_A.editar_evento();
        Eventos_A.editar_campos();
        Eventos_A.cancelar_edicao();
        Eventos_A.assertiva_modal_cancelamento();
        Eventos_A.confirmar_cancelamento();
    })
})