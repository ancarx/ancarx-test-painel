/// <references="Cypress" />
import { Basic } from "../../support/pages/basics/base.page";
import { Eventos } from "../../support/pages/eventos/eventos-criar.page";
import { Eventos_F } from "../../support/pages/eventos/eventos-filtrar.page";

describe('EVENTOS - Filtrar um evento', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/event')
        Basic.selecionar_shopping_sna();
    })
    it('Deve filtrar eventos e fazer assertiva', () =>{
        Eventos.assertiva_pag_eventos();
        Eventos_F.acessar_filtro();
        Eventos_F.visualizar_modal();
        Eventos_F.botao_limpar_existe();
        Eventos_F.preencher_filtro();
        Eventos_F.clicar_filtrar();
    })
})