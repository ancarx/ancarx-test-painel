import { Pesquisas } from "../../../support/pages/app/pesquisas/app-pesquisas.page"
import {Basic} from "../../../support/pages/basics/base.page"


describe('PESQUISAS - Ver detalhes', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/survey')
        Basic.selecionar_shopping_sna();
    })
    it('Deve fazer assertiva na página e nos registros', () =>{
        Pesquisas.assertiva_pagina();
        Pesquisas.assertiva_registros();
    })
    it('Deve cancelar uma pesquisa', () =>{
        Pesquisas.btn_cancelar_pesquisa();
        Pesquisas.assrtv_modal_cancelar();
        Pesquisas.cancelar_pesquisa();
    })
})