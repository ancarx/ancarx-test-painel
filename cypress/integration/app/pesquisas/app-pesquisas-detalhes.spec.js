import {Basic} from "../../../support/pages/basics/base.page"
import { Pesquisas_detalhes } from "../../../support/pages/app/pesquisas/app-pesquisas-detalhes.page"

describe('PESQUISAS - Ver detalhes', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/survey')
        Basic.selecionar_shopping_sna();
    })
    it('Deve pegar Nome, URL, Data Inicial e Data Final da pesquisa e comparar com detalhe', () =>{
        Pesquisas_detalhes.pega_valores_e_compara_com_detalhe()
    })
})