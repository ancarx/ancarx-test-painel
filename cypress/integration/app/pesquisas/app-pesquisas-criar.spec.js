import { Basic } from "../../../support/pages/basics/base.page";
import { Pesquisas_criar } from "../../../support/pages/app/app-pesquisas-criar.page";


describe('PESQUISAS - Criar uma pesquisa', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/survey')
        Basic.selecionar_shopping_sna();
    })
    it('Deve criar uma nova pesquisa e salvar rascunho', () =>{
        Pesquisas_criar.btn_criar_pesquisa();
        Pesquisas_criar.assertiva_pagina();
        Pesquisas_criar.preencher_pesquisa();
        Pesquisas_criar.btn_page('Salvar rascunho');
        Pesquisas_criar.assrtv_modal('Rascunho salvo com sucesso!');
        Pesquisas_criar.concluir_fluxo();
    })
    it('Deve criar uma nova pesquisa, salvar e publicar', () =>{
        Pesquisas_criar.btn_criar_pesquisa();
        Pesquisas_criar.assertiva_pagina();
        Pesquisas_criar.preencher_pesquisa();
        Pesquisas_criar.btn_page('Salvar e publicar');
        Pesquisas_criar.assrtv_modal('Pesquisa salva e publicada com sucesso!');
        Pesquisas_criar.concluir_fluxo();
    })
    it('Deve cancelar a criação de uma nova pesquisa', () =>{
        Pesquisas_criar.btn_criar_pesquisa();
        Pesquisas_criar.assertiva_pagina();
        Pesquisas_criar.preencher_pesquisa();
        Pesquisas_criar.btn_page('Cancelar');
        Pesquisas_criar.assrtv_modal_cancelar();
        Pesquisas_criar.concluir_fluxo_cancelar('2');
    })
})