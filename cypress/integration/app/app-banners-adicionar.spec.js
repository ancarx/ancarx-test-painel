import { Basic } from "../../support/pages/basics/base.page";
import { Banners_adicionar } from "../../support/pages/app/app-banners-adicionar.pages";


describe('BANNERS - Adicionar um novo banner', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/shopping-banner')
        Basic.selecionar_shopping_sna();
    })
    it('Deve adicionar um novo banner c/ link interno', () =>{
        Banners_adicionar.delete_se_necessário();
        Banners_adicionar.adicionar_banner();
        Banners_adicionar.assrtv_modal();
        Banners_adicionar.upar_imagem();
        Banners_adicionar.selecionar_link('Interno');
        cy.percySnapshot('Criação de banner com fluxo interno');
        Banners_adicionar.fluxo_interno();
        Banners_adicionar.botão_modal('Adicionar');
        Banners_adicionar.assrtv_modal_final_sucesso();
        Banners_adicionar.completar_fluxo('Voltar para página de banners');
    })
    it('Deve adicionar um novo banner c/ link externo', () =>{
        Banners_adicionar.delete_se_necessário();
        Banners_adicionar.adicionar_banner();
        Banners_adicionar.assrtv_modal();
        Banners_adicionar.upar_imagem();
        Banners_adicionar.selecionar_link('Externo');
        cy.percySnapshot('Criação de banner com fluxo Externo');
        Banners_adicionar.fluxo_externo('https://management-panel-dot-ancarx-hom.rj.r.appspot.com/home');
        Banners_adicionar.botão_modal('Adicionar');
        Banners_adicionar.assrtv_modal_final_sucesso();
        Banners_adicionar.completar_fluxo('Voltar para página de banners');
    })
    it('Deve desistir de adicionar um novo banner', () =>{
        Banners_adicionar.delete_se_necessário();
        Banners_adicionar.adicionar_banner();
        Banners_adicionar.assrtv_modal();
        Banners_adicionar.upar_imagem();
        Banners_adicionar.selecionar_link('Externo');
        Banners_adicionar.fluxo_externo('https://management-panel-dot-ancarx-hom.rj.r.appspot.com/home');
        Banners_adicionar.botão_modal('Cancelar');
        Banners_adicionar.assrtv_modal_final_desistencia();
        cy.percySnapshot('Desistência de criação de banner');
        Banners_adicionar.completar_fluxo('Sim, cancelar');
    })
})