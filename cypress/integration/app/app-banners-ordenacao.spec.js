import { Basic } from "../../support/pages/basics/base.page";
import { Banners_ordenar } from "../../support/pages/app/app-banners-ordenacao.page";


describe('BANNERS - Ordenar um banner', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/shopping-banner')
        Basic.selecionar_shopping_sna();
    })
    it('Deve ordenar o primeiro banner 2x', () =>{
        cy.percySnapshot(
            'Página banners'/* ,
            {
                percyCSS: `.styles__GlobalStyleDiv-sc-1a1upx4-0 .iNgSw {display:none}`, 
                percyCSS: `.styles__GlobalStyleDiv-sc-1a1upx4-0 .dFRYly {display:none}`
            } */);
        Banners_ordenar.assertiva_pagina();
        Banners_ordenar.editar_ordenacao();
        cy.percySnapshot('Ordenação da página de banners');
        Banners_ordenar.ordenar_banners();
        Banners_ordenar.salvar_ordenação();
        Banners_ordenar.assrtv_ordenacao_sucesso();
        Banners_ordenar.completar_fluxo_sucesso();
    })
    it('Deve desistir de ordenar banners', () =>{
        Banners_ordenar.assertiva_pagina();
        Banners_ordenar.editar_ordenacao();
        Banners_ordenar.ordenar_banners();
        Banners_ordenar.cancelar_ordenação();
        cy.percySnapshot('Desistência de editar banners');
        Banners_ordenar.assrtv_ordenacao_falha();
        Banners_ordenar.completar_fluxo_falha();
    })
    it('Deve excluir o primeiro banner', () =>{
        Banners_ordenar.excluir_banner();
        Banners_ordenar.assrtv_exclusão();
        cy.percySnapshot('Exclusão de Banner');
        Banners_ordenar.completar_exclusão();
    })
})