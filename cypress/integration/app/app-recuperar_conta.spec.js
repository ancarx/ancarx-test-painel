import { Basic } from "../../support/pages/basics/base.page";
import { Recuperar_conta } from "../../support/pages/app/app-recuperar_conta.page"

describe('RECUPERAR CONTA - deve recuperar uma conta do usuário', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/recover-account')
        Basic.selecionar_shopping_sna();
    })
    it('Deve consultar e recuperar acesso de uma conta', () =>{
        Recuperar_conta.assertiva_pag();
        cy.percySnapshot('Página de recuperação de senha');
        Recuperar_conta.consultar_cpf('38941280893');
        Recuperar_conta.assrtv_consulta();
        Recuperar_conta.recuperar_acesso();
        Recuperar_conta.assertiva_modal();
        cy.percySnapshot('Modal de recuperação');
        Recuperar_conta.preencher_modal();
        Recuperar_conta.assrtv_sucesso();
        cy.percySnapshot('Recuperação de senha com sucesso');
    })
    it('Deve consultar um cpf inválido', () =>{
        Recuperar_conta.assertiva_pag();
        Recuperar_conta.consultar_cpf('06570944045');
        Recuperar_conta.assrtv_cpf_não_encontrado();
        cy.percySnapshot('CPF não encontrado');
        Recuperar_conta.nova_consulta();
    })
})