import { Basic } from "../../support/pages/basics/base.page";
import { Banners_alterar } from "../../support/pages/app/app-banners-alterar.pages";


describe('BANNERS - Alterar um banner', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/shopping-banner')
        Basic.selecionar_shopping_sna();
    })
    it('Deve alterar um banner c/ link interno', () =>{
        Banners_alterar.alterar_banner();
        Banners_alterar.assrtv_modal();
        Banners_alterar.selecionar_link('Interno');
        Banners_alterar.fluxo_interno();
        Banners_alterar.salvar_edicao();
        Banners_alterar.assrtv_modal_final_sucesso();
        cy.percySnapshot();
        Banners_alterar.completar_fluxo('Voltar para página de banners');
        Banners_alterar.assrtv_alteracao('Interno');
    })
    it('Deve alterar um banner c/ link externo', () =>{
        Banners_alterar.alterar_banner();
        Banners_alterar.assrtv_modal();
        Banners_alterar.selecionar_link('Externo');
        Banners_alterar.fluxo_externo('https://management-panel-dot-ancarx-hom.rj.r.appspot.com/home');
        Banners_alterar.salvar_edicao();
        Banners_alterar.assrtv_modal_final_sucesso();
        cy.percySnapshot();
        Banners_alterar.completar_fluxo('Voltar para página de banners');
        Banners_alterar.assrtv_alteracao('Externo');
    })
})