import {Basic} from '../../support/pages/basics/base.page'
import {Promotoria} from '../../support/pages/promotoria/promotoria-checkin.pages'

describe('PROMOTORIA - Check-in de eventos', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/event-promoter')
        Basic.selecionar_shopping_sna();
    })
    /* it('Deve fazer checkin no evento', () =>{
        Promotoria.assertiva_pagina();
        Promotoria.escolher_evento();
        Promotoria.escolhe_sessão();
        Promotoria.assertiva_modal_codigo();
        Promotoria.inserir_codigo('944D6');
        Promotoria.validar();
        Promotoria.assertiva_modal_ingresso();
        Promotoria.liberar_acesso();
        Promotoria.assertiva_modal_confirmação();
        Promotoria.nova_consulta();
    }) */
    describe('Deve falhar ao fazer checkin', () =>{
        it('Preenchimento obrigatório', () =>{
            Promotoria.assertiva_pagina();
            Promotoria.escolher_evento();
            Promotoria.escolhe_sessão();
            Promotoria.assertiva_modal_codigo();
            Promotoria.inserir_codigo(' ');
            Promotoria.validar();
            Promotoria.msg_erro('Preenchimento obrigatório');
        })
        it('Código incorreto', () =>{
            Promotoria.assertiva_pagina();
            Promotoria.escolher_evento();
            Promotoria.escolhe_sessão();
            Promotoria.assertiva_modal_codigo();
            Promotoria.inserir_codigo('D57');
            Promotoria.validar();
            Promotoria.msg_erro('Código incorreto!');
        })
    })
})