/// <references = "Cypress" />
import {Basic} from '../../support/pages/basics/base.page'
import {Login} from '../../support/pages/login/login.page'

describe('LOGIN - Faz login', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        Basic.acessa_hom();
        Basic.reload_page();
    })
    it('Faz login c/ sucesso', () =>{
        Login.preenche_login();
        Login.clica_entrar();
        Login.assertiva_login_sucess();
    })
    it('Faz login s/ sucesso', () =>{
        Login.preenche_login_falho();
        Login.visualizar_senha();
        Login.clica_entrar();
        Login.assertiva_login_falho()
    })
})