import {Basic} from '../../../support/pages/basics/base.page'
import {Shopping_S} from '../../../support/pages/shoppings/servicos/shopping-servicos.pages'
import {Shopping_F} from '../../../support/pages/shoppings/shopping-filtrar.page'


describe('SHOPPINGS - Serviços', () =>{
    const dataTransfer = new DataTransfer;
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
    })
    it('Deve fazer assertiva na página inteira', () =>{
        Shopping_F.filtrar('Shopping Nova America')
        Shopping_S.acessar_servicos() 
        Shopping_S.assrtv_pag_servicos()
    })
    it('Deve editar a ordenação', () =>{
        Shopping_F.filtrar('Shopping Nova America')
        Shopping_S.acessar_servicos()
        Shopping_S.editar_ordenacao();
        Shopping_S.drag_and_drop();
    })
    it('Deve fazer acessar os detalhes de um serviço', () =>{
        Shopping_F.filtrar('Shopping Nova America')
        Shopping_S.acessar_servicos()
        Shopping_S.ver_detalhes()
        Shopping_S.voltar_breadcrumbs()
        
    })
})