import {Shopping_S} from '../../../support/pages/shoppings/servicos/shopping-servicos.pages'
import {Shopping_SA} from '../../../support/pages/shoppings/servicos/shopping-servicos-alterar.pages'
import {Shopping_F} from '../../../support/pages/shoppings/shopping-filtrar.page'
import {Shopping} from '../../../support/pages/shoppings/shopping-listar.page'
import {Basic} from '../../../support/pages/basics/base.page'
describe('SERVIÇOS - Deve alterar um serviço', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
        Shopping.assertiva_pag();
        Shopping_F.filtrar('Nova América');
    })
    it('Deve alterar um serviço', () =>{
        Shopping_S.acessar_servicos();
        Shopping_S.assrtv_pag_servicos();
        Shopping_SA.btn_alterar();
        Shopping_SA.assrtv_pagina();
        Shopping_SA.preencher_campos();
        Shopping_SA.salvar_alteração();
        Shopping_SA.assrtv_modal();
        Shopping_SA.completar_fluxo();
    })
})