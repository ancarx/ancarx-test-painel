import {Shopping_FAQC} from '../../../support/pages/shoppings/faq/shopping-faq-criar.pages'
import {Shopping_FAQ} from '../../../support/pages/shoppings/faq/shopping-faq-listar.pages'
import {Shopping_F} from '../../../support/pages/shoppings/shopping-filtrar.page'
import {Shopping} from '../../../support/pages/shoppings/shopping-listar.page'
import {Basic} from '../../../support/pages/basics/base.page'
describe('FAQ - Deve criar um novo FAQ', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
        Shopping.assertiva_pag();
        Shopping_F.filtrar('Nova América');
    })
    it('Deve criar um faq', () =>{
        Shopping_FAQ.selecionar_faq();
        Shopping_FAQ.assrtv_pagina();
        Shopping_FAQC.criar_faq();
        Shopping_FAQC.assrtv_pag();
        Shopping_FAQC.preencher_campos();
        Shopping_FAQC.adicionar_faq();
        Shopping_FAQC.assrtv_modal();
        Shopping_FAQC.confirmar_faq();
    })
})