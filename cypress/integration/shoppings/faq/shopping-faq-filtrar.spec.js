import {Shopping_FAQ} from '../../../support/pages/shoppings/faq/shopping-faq-listar.pages'
import {Shopping_F} from '../../../support/pages/shoppings/shopping-filtrar.page'
import {Shopping} from '../../../support/pages/shoppings/shopping-listar.page'
import {Basic} from '../../../support/pages/basics/base.page'
import { Shopping_FAQF } from '../../../support/pages/shoppings/faq/shopping-faq-filtrar.pages'
describe('FAQ - Deve filtrar FAQ', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
        Shopping.assertiva_pag();
        Shopping_F.filtrar('Nova América');
    })
    it('Deve filtrar um faq', () =>{
        Shopping_FAQ.selecionar_faq();
        Shopping_FAQF.filtrar_faq();
        Shopping_FAQF.assrtv_modal();
        Shopping_FAQF.filtrar_e_fazer_assertiva();
    })
    it('Deve limpar filtro', () =>{
        Shopping_FAQ.selecionar_faq();
        Shopping_FAQF.filtrar_faq();
        Shopping_FAQF.assrtv_modal();
        Shopping_FAQF.preencher_modal();
        Shopping_FAQF.limpar_filtro();
    })
    it('Deve buscar um faq', () =>{
        Shopping_FAQ.selecionar_faq();
        Shopping_FAQF.buscar_faq();
        Shopping_FAQF.assrtv_busca();
    })
})