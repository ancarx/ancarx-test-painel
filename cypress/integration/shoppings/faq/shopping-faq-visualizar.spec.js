import {Shopping_FAQ} from '../../../support/pages/shoppings/faq/shopping-faq-listar.pages'
import {Shopping_F} from '../../../support/pages/shoppings/shopping-filtrar.page'
import {Shopping} from '../../../support/pages/shoppings/shopping-listar.page'
import {Basic} from '../../../support/pages/basics/base.page'
import {Shopping_FAQV} from '../../../support/pages/shoppings/faq/shopping-faq-visualizar.pages'

describe('FAQ - Deve visualizar detalhes de um FAQ', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
        Shopping.assertiva_pag();
        Shopping_F.filtrar('Nova América');
    })
    it('Deve visualizar detalhes de um FA', () =>{
        Shopping_FAQ.selecionar_faq();
        Shopping_FAQV.pega_valores_e_compara_com_detalhe();
        Shopping_FAQV.assrtv_pagina();
    })
})