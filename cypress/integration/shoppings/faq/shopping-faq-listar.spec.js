import {Shopping_FAQ} from '../../../support/pages/shoppings/faq/shopping-faq-listar.pages'
import {Shopping_F} from '../../../support/pages/shoppings/shopping-filtrar.page'
import {Shopping} from '../../../support/pages/shoppings/shopping-listar.page'
import {Basic} from '../../../support/pages/basics/base.page'
describe('FAQ - Deve listar FAQ', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
        Shopping.assertiva_pag();
        Shopping_F.filtrar('Nova América');
    })
    it('Deve listar faq', () =>{
        Shopping_FAQ.selecionar_faq();
        Shopping_FAQ.assrtv_pagina();
        Shopping_FAQ.listar_faq();
    })
})