import {Shopping_FAQ} from '../../../support/pages/shoppings/faq/shopping-faq-listar.pages'
import {Shopping_F} from '../../../support/pages/shoppings/shopping-filtrar.page'
import {Shopping} from '../../../support/pages/shoppings/shopping-listar.page'
import {Basic} from '../../../support/pages/basics/base.page'
import { Shopping_FAQA } from '../../../support/pages/shoppings/faq/shopping-faq-alterar.pages'
import { Shopping_FAQV } from '../../../support/pages/shoppings/faq/shopping-faq-visualizar.pages'

const faker = require ('faker')
var loremWords = faker.lorem.words(3)
var loremSentences = faker.lorem.sentence()
describe('FAQ - Deve alterar FAQ', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
        Shopping.assertiva_pag();
        Shopping_F.filtrar('Nova América');
    })
    it('Deve alterar um faq', () =>{
        Shopping_FAQ.selecionar_faq();
        Shopping_FAQV.ver_detalhes();
        Shopping_FAQA.editar_resposta();
        Shopping_FAQA.assrtv_pagina();
        Shopping_FAQA.preencher_campos( 'AUT - PERGUNTA: '+loremWords,
                                        'AUT - '+loremSentences,
                                        'AUT - '+loremSentences);
        Shopping_FAQA.botao_final('2');
        Shopping_FAQA.assrtv_sucesso();
        Shopping_FAQA.completar_fluxo();
    })
    it('Deve falhar ao alterar um faq', () =>{
        Shopping_FAQ.selecionar_faq();
        Shopping_FAQV.ver_detalhes();
        Shopping_FAQA.editar_resposta();
        Shopping_FAQA.assrtv_pagina();
        Shopping_FAQA.preencher_campos( ' ',
                                        ' ',
                                        ' ');
        Shopping_FAQA.botao_final('2');
        Shopping_FAQA.assrtv_falha();
    })
    it('Deve desistir de alterar um faq', () =>{
        Shopping_FAQ.selecionar_faq();
        Shopping_FAQV.ver_detalhes();
        Shopping_FAQA.editar_resposta();
        Shopping_FAQA.assrtv_pagina();
        Shopping_FAQA.botao_final('1');
        Shopping_FAQA.assrtv_desistencia();
        Shopping_FAQA.completar_desistencia();
    })
})