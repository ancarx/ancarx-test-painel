import {Basic} from '../../support/pages/basics/base.page'
import {Shopping} from '../../support/pages/shoppings/shopping-listar.page'

describe('SHOPPINGS - Deve fazer assertiva na página', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
    })
    it('Deve fazer assertiva na página inteira', () =>{
        Shopping.assertiva_pag();
    })
})