import {Shopping_Listar_Piso} from '../../support/pages/shoppings/shopping-editar-piso-listar.pages'
import {Shopping_Edit_Piso} from '../../support/pages/shoppings/shopping-editar-piso-alterar.pages'
import {Shopping_F} from '../../support/pages/shoppings/shopping-filtrar.page'
import {Shopping} from '../../support/pages/shoppings/shopping-listar.page'
import {Basic} from '../../support/pages/basics/base.page'
describe('PISO - Deve listar os pisos', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
        Shopping.assertiva_pag();
        Shopping_F.filtrar('Nova América');
    })
    it('Deve listar os pisos', () =>{
        Shopping_Listar_Piso.editar_shopping();
        Shopping_Listar_Piso.aba_pisos();
        Shopping_Listar_Piso.assrtv_modal();
        Shopping_Edit_Piso.btn_editar_piso();
        Shopping_Edit_Piso.preencher_edicao();
        Shopping_Edit_Piso.alterar_edicao();
        Shopping_Edit_Piso.assrtv_modal();
        Shopping_Edit_Piso.terminar_fluxo();
    })
})