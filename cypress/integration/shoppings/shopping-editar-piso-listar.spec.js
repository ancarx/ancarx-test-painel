import {Shopping_Listar_Piso} from '../../support/pages/shoppings/shopping-editar-piso-listar.pages'
import {Shopping_F} from '../../support/pages/shoppings/shopping-filtrar.page'
import {Shopping} from '../../support/pages/shoppings/shopping-listar.page'
import {Basic} from '../../support/pages/basics/base.page'
describe('PISO - Deve listar os piso', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
        Shopping.assertiva_pag();
        Shopping_F.filtrar('Nova América');
    })
    it('Deve listar os pisos', () =>{
        Shopping_Listar_Piso.editar_shopping();
        Shopping_Listar_Piso.aba_pisos();
        Shopping_Listar_Piso.assrtv_modal();
       /*  Shopping_Listar_Piso.criar_piso();
        Shopping_Listar_Piso.editar_shopping();
        Shopping_Listar_Piso.aba_pisos(); */
        Shopping_Listar_Piso.assrtv_pisos();
    })
})