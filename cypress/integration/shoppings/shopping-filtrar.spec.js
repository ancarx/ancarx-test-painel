import {Basic} from '../../support/pages/basics/base.page'
import {Shopping} from '../../support/pages/shoppings/shopping-listar.page'
import {Shopping_F} from '../../support/pages/shoppings/shopping-filtrar.page'

describe('SHOPPINGS - Deve filtrar shopping através do nome/sigla', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login();
        cy.visit('/shopping')
        Basic.selecionar_shopping_sna();
    })
    it('Deve filtrar por nome', () =>{
        Shopping.assertiva_pag();
        Shopping_F.filtrar('Shopping');
        Shopping_F.resultados_nome();
    })
    it('Deve filtrar por sigla', () =>{
        Shopping.assertiva_pag();
        Shopping_F.filtrar('SNA');
        Shopping_F.resultados_sigla();
    })
})