import { Basic } from "../../support/pages/basics/base.page";
import {Campanhas_Filtrar} from '../../support/pages/campanhas/campanhas-filtrar.pages';
import {Campanhas} from "../../support/pages/campanhas/campanhas-listar.pages";

describe('CAMPANHAS - Filtrar uma campanha', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/campaign')
        Basic.selecionar_shopping_sna();
    })
    it('Deve filtrar uma campanha', () =>{
        Campanhas.assertiva_pag_campanhas();
        Campanhas_Filtrar.filtrar_campanha();
        Campanhas_Filtrar.assertiva_modal();
        Campanhas_Filtrar.filtrar_por_situacao();
        Campanhas_Filtrar.assertiva_filtro_campanha();
    })
})