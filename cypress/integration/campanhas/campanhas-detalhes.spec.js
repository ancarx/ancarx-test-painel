import { Basic } from "../../support/pages/basics/base.page";
import { Campanhas } from "../../support/pages/campanhas/campanhas-listar.pages";
import { Campanhas_Detalhes } from "../../support/pages/campanhas/campanhas-detalhes.pages"

describe('CAMPANHAS - Conferir detalhes de uma campanha', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/campaign')
        Basic.selecionar_shopping_sna();
    })
    it('Deve conferir os detalhes de uma campanha', () =>{
        Campanhas.assertiva_pag_campanhas();
        Campanhas_Detalhes.pega_valores_e_compara_com_detalhe();
        Campanhas_Detalhes.assertiva_pag_detalhes();
    })
})