import { Basic } from "../../support/pages/basics/base.page";
import {Campanhas_Criar} from "../../support/pages/campanhas/campanhas-criar.pages";
import {Campanhas} from "../../support/pages/campanhas/campanhas-listar.pages"

describe('CAMPANHAS - Criar uma nova campanha', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/campaign')
        Basic.selecionar_shopping_sna();
    })
    it('Deve criar uma nova campanha', () =>{
        Campanhas_Criar.criar_campanha();
        Campanhas_Criar.assertiva_pagina_criar();
        Campanhas_Criar.preencher_dados();
        Campanhas_Criar.upar_imagem();
        Campanhas_Criar.salvar_publicar();
        Campanhas_Criar.modal_sucesso();
        
        //Assertiva feita para ver se foi cadastrada a campanha
        Campanhas.assertiva_pag_campanhas();
        Campanhas_Criar.buscar_campanha(); 
    })
    it('Deve falhar em criar uma nova campanha', () =>{
        Campanhas_Criar.criar_campanha();
        Campanhas_Criar.assertiva_pagina_criar();
        Campanhas_Criar.preencher_dados();
        Campanhas_Criar.upar_imagem();
        Campanhas_Criar.cancelar();
        Campanhas_Criar.modal_falha();
        Campanhas_Criar.confirmar_cancelar();

        //Assertiva feita para ver se voltou para página principal
        Campanhas.assertiva_pag_campanhas();
    })
})