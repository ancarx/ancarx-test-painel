import { Basic } from "../../support/pages/basics/base.page";
import { Campanhas_Alterar } from "../../support/pages/campanhas/campanhas-alterar.pages";
import { Campanhas } from "../../support/pages/campanhas/campanhas-listar.pages"

describe('CAMPANHAS - Alterar uma campanha', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/campaign')
        Basic.selecionar_shopping_sna();
    })
    it('Deve alterar uma campanha', () =>{
        Campanhas.assertiva_pag_campanhas();
        Campanhas_Alterar.alterar_campanha();
        Campanhas_Alterar.alterar_nome();
        Campanhas_Alterar.salvar_edicao();
        Campanhas_Alterar.modal_sucesso();
        Campanhas_Alterar.voltar_pra_lista();
        Campanhas_Alterar.buscar_campanha();
        Campanhas_Alterar.assertiva_campanha();
    })
})