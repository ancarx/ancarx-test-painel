import { Basic } from "../../support/pages/basics/base.page";
import {Campanhas} from "../../support/pages/campanhas/campanhas-listar.pages"

describe('CAMPANHAS - Listar as campanhas cadastradas', () =>{
    beforeEach('Acessa ambiente de homologação', () =>{
        cy.login()
        cy.visit('/campaign')
        Basic.selecionar_shopping_sna();
    })
    it('Deve listar todas as campanhas cadastradas', () =>{
        Campanhas.assertiva_pag_campanhas();
        Campanhas.assertiva_registro_campanhas();
    })
})