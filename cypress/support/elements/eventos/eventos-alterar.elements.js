export const eventos_alt_ele = {
    BTN_EDITAR:'//tbody/tr[contains(.,"(NÃO EXCLUIR) AUTOMAÇÃO")]/td[7]/div/button[2]',
    NOME_EVENTO: '#event-name',
    DESCRICAO_APP: '#event-description-app-site',
    REGULAMENTOS_REGRAS: '#event-rules',
    BTN_SALVAR_EDICAO: '//div[@class="container"]/div[1]/div[2]//span[contains(.,"Salvar edição")]/..',
    BTN_CANCELAR_EDICAO: '//div[@class="container"]/div[1]/div[2]//span[contains(.,"Cancelar")]/..',
    MODAL: '//div[@data-testid="modal"]/div[2]',
    ASSRTV_TEXTO_MODAL: '//div[@data-testid="modal"]/div[2]//p',
    ASSRTV_TEXT_MODAL_DESISTENCIA: '//div[@data-testid="modal"]/div[2]//h1',
    BTN_CONFIRM_CANCELAMENTO: '//div[@class="btn-modal-actions"]/button[2]/span[contains(.,"Sim, cancelar")]',
    BTN_DESISTIR_CANCELAMENTO: '//div[@class="btn-modal-actions"]/button[1]/span[contains(.,"Desistir")]',
    TITULO_PAGINA: '//div[@class="container"]//h1[contains(.,"Lista de Eventos")]',
    BTN_CONFIRM_EDICAO: '//div[@data-testid="modal"]/div[2]//button'
}