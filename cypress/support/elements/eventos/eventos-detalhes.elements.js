export const eventos_detalhes_ele = {
    DETALHES: nome => `//tbody/tr[contains(.,"${nome}")][1]/td[7]/div/button[3]/span`, 
    THUMB_EVENTO: '//tbody/tr[1]/td[1]/div/img',
    NOME_EVENTO: '//tbody/tr[1]/td[2]/div/div',
    SITUACAO_EVENTO: '//tbody/tr[1]/td[3]/div/div',
    INICIO_EVENTO: '//tbody/tr[1]/td[5]/span',
    FIM_EVENTO: '//tbody/tr[1]/td[6]/span',
    THUMB_DETALHE: '//img[@alt="thumb"]',
    NOME_EVENTO_DETALHE: '//div[@class="container"]/div/div[3]/div[1]/span[2]',
    SITUACAO_EVENTO_DETALHE: '//div[@class="container"]/div/div[3]/div[3]/span[2]',
    DATA_SESSAO_DETALHE: '//div[@class="styles__GlobalStyleDiv-sc-1a1upx4-0 idTQPD"]/div[10]/div[2]/span[2]',
    BREADCRUMB_EVENTOS: '//a[@href="/event"]/span[contains(.,"Lista de Eventos")]',


    //Listar os eventos do shopping SNA
    TITULO_PAGINA: '//div[@class="styles__GlobalStyleDiv-sc-1a1upx4-0 jbNxLr"]/h1[@class="styles__GlobalStyleH1-sc-1a1upx4-5 iHekCf"]',
    BTN_CRIAR_EVENTO: '//div[@class="styles__GlobalStyleDiv-sc-1a1upx4-0 hgcxPt"]/button[1]//span[contains(.,"Criar evento")]',
   
}