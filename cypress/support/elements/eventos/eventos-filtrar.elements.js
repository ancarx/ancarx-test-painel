export const eventos_filtro_ele = {
    ASSRTV_MODAL: '//div[@data-testid="modal"]/div[2]',
    BTN_FILTRO: '//div[@data-testid="modal"]/div[2]//button[@type="submit"]',
    BTN_LIMPAR_FILTRO: '//div[@data-testid="modal"]/div[2]//button[@type="button"]',
    SITUACAO: '#status',
    SITUACAO_SELECT: situacao =>`//div[@id="status"]//div/ul/li[contains(.,"${situacao}")]`,
    ASSRTV_SITUACAO_DOS_EVENTOS: '//tbody[@class="styles__GlobalStyleTBody-sc-1a1upx4-14 bnmAtZ tbody"]/tr/td[3]/div/div',
    CONTAINER: '//div[@class="container"]//div[@data-testid="card"]/div[2]',
    MSG_CONTAINER: '//div[@data-testid="notfound"]//p',
    PERIODO_INICIAL: '#start-date',
    PERIODO_FINAL: '#end-date'
}