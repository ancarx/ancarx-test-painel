export const campanhas_filtrar_ele = {
    MODAL_FILTRAR: '//div[@data-testid="modal"]/div[2]',
    DATA_INICIAL: '#start-date',
    DATA_FINAL: '#end-date',
    SITUACAO: '#status',
    SITUACAO_SELECT: situacao => `//div[@id="status"]/div/div//li[contains(.,"${situacao}")]`,
    EM_DESTAQUE: '#hightlight',
    EM_DESTAQUE_SELECT: destaque => `//div[@id="hightlight"]/div/div//li[contains(.,"${destaque}")]`,
    BTN_MODAL: opcao => `//div[@data-testid="modal"]/div[2]//button[@data-testid="button"][contains(.,"${opcao}")]`
}