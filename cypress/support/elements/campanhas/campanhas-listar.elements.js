export const campanhas_ele = {
    TITULO_PAG: '//div[@data-testid="card"]/div[1]//h1[contains(.,"Lista de Campanhas")]',
    BTN_CRIAR_CAMPANHA: '//div[@data-testid="card"]/div[1]/div[2]//button[contains(.,"Criar Campanha")]',
    INP_BUSCAR: '#search-user',
    BTN_FILTRAR: '//div[@data-testid="card"]/div[1]/div[2]//button[contains(.,"Filtrar")]',

    // FILTROS
    FILTRO_IMAGEM: '//table[@data-testid="table"]//th[contains(.,"Imagem")]',
    FILTRO_CAMPANHA: '//table[@data-testid="table"]//th[contains(.,"Nome da Campanha")]',
    FILTRO_SITUACAO: '//table[@data-testid="table"]//th[contains(.,"Situação")]',
    FILTRO_DATA_INICIAL: '//table[@data-testid="table"]//th[contains(.,"Data Inicial")]',
    FILTRO_DATA_FINAL: '//table[@data-testid="table"]//th[contains(.,"Data Final")]',
    FILTRO_DESTACADO: '//table[@data-testid="table"]//th[contains(.,"Destacado")]',
    FILTRO_LIMITE_CUPONS: '//table[@data-testid="table"]//th[contains(.,"Limite cupons p/ CPF")]',
    FILTRO_OFERTAS: '//table[@data-testid="table"]//th[contains(.,"Ofertas vinculadas")]',

    // DADOS DE REGISTRO
    REG_THUMB: '//table[@data-testid="table"]/tbody/tr[1]/td[1]',
    REG_NOME: length => `//table[@data-testid="table"]/tbody/tr${length}/td[2]`,
    REG_SITUACAO: length => `//table[@data-testid="table"]/tbody/tr${length}/td[3]/div/div`,
    REG_DATA_INICIAL: '//table[@data-testid="table"]/tbody/tr[1]/td[4]',
    REG_DATA_FINAL: '//table[@data-testid="table"]/tbody/tr[1]/td[5]',
    REG_DESTACADO: '//table[@data-testid="table"]/tbody/tr[1]/td[6]',
    REG_LIMITE_CUPONS: '//table[@data-testid="table"]/tbody/tr[1]/td[7]',
    REG_OFERTAS_VINCULADAS: '//table[@data-testid="table"]/tbody/tr[1]/td[8]',
    BTN_EDITAR: contains => `//table[@data-testid="table"]/tbody/tr[contains(.,"AUTOMAÇÃO")][1]/td[9]/div/button[2]`,
    BTN_VISUALIZAR: '//table[@data-testid="table"]/tbody/tr[1]/td[9]//button[3]'
}