export const campanhas_detalhes_ele = {
    DETALHES_DADOS: length => `//div[@class="container"]//div[3]/div[${length}]/span[2]`,
    DETALHES_THUMB: '//img[@alt="url"]',
    DETALHES_SESSÕES: length => `//div[@class="container"]//div[3]/div[${length}]/span[1]`,
    DETALHES_DESCRICAO: '//div[@class="container"]//div[3]/div[7]//p'
}