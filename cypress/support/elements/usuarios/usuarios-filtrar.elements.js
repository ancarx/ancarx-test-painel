export const usuarios_filtro_ele = {
    BTN_FILTRAR: '//div[@class="container"]/div[@data-testid="card"]/div[1]/div[2]/div[1]//button[2]/span/..',
    //MODAL DO FILTRO
    MODAL_FILTRO: '//div[@data-testid="modal"]/div[2]',
    TITULO_MODAL: '//div[@data-testid="modal"]/div[2]/div[1]/h1[contains(.,"Filtrar")]',
    PERFIL: '#filter-roles',
    PERFIL_SELECT: '#Admin0', //Seleciona o Super Admin
    SHOPPINGS: '#filter-shoppings',
    SHOPPINGS_SELECT: '#SNA18', //Seleciona o shopping SNA
    USUARIOS: '//div[@data-testid="modal"]/div[2]/div[2]/form/h1[contains(.,"Usuários")]',
    USUARIOS_SELECT: '#active', //Seleciona usuário ativo
    BTN_LIMPAR_FILTRO: '//div[@data-testid="modal"]/div[2]/div/form/div[3]//span[contains(.,"Limpar Filtro")]',
    BTN_FILTRAR_MODAL: '//div[@data-testid="modal"]/div[2]/div/form/div[3]//span[contains(.,"Filtrar")]',
    
    //ASSERTIVA PRODUTOS DO FILTRO
    SHOPPING_FILTRADO: '//tbody/tr/td[4]/div//div',
    SITUACAO_FILTRADO: '//tbody/tr/td[5]',
    PERFIL_FILTRADO: '//tbody/tr/td[3]',

    RETICENCIAS: (i) => `//tbody/tr[${i}]/td[4]/div//div/span`,
    ANOTHER_SHOPPING: '//tbody/tr/td[4]/div//div/span/../../div[2]/ul/li'

}