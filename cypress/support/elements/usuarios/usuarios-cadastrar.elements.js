export const usuarios_ele = {
    // ASSERTIVA MODAL
    MODAL: '//div[@data-testid="modal"]/div[2]',
    TITULO_MODAL: '//div[@data-testid="modal"]/div[2]/div[1]/h1',
    USUARIO_NOME: '//div[@data-testid="modal"]/div[2]//input[@id="user-name"]',
    USUARIO_EMAIL: '//div[@data-testid="modal"]/div[2]//input[@id="user-email"]',
    USUARIO_PERFIL: '//div[@data-testid="modal"]/div[2]//div[@id="profile-user"]',
    USUARIO_PERFIL_SELECT: perfil => `//div[@data-testid="modal"]/div[2]//div[@id="profile-user"]/div/div/ul/li[contains(.,"${perfil}")]`,
    USUARIO_SHOPPING: '//div[@data-testid="modal"]/div[2]//div[@id="shoppings-user"]',
    USUARIO_SHOPPING_SELECT: '#SNA18',
    USUARIO_SHOPPING_SEL_LOJISTA: '//div[@id="shoppings-user"]/div//ul[contains(.,"SNA")]',
    USER_PASSWORD: "#user-password",
    USER_CONFIRM_PASSWORD: '#user-confirm-password',
    BTN_CRIAR: '//div[@data-testid="modal"]/div[2]/div[2]//span[contains(.,"Criar")]',
    STORE: '#store',
    STORE_SELECT: '//input[@id="store"]/../../div[3]//li[1]',

    // ASSERTIVA PÁGINA
    TITULO_PAGINA: '//div[@class="container"]/div[@data-testid="card"]//h1',
    FILTRO_NOME: '//thead/tr//th[1]',
    FILTRO_EMAIL: '//thead/tr//th[2]',
    FILTRO_PERFIL: '//thead/tr//th[3]',
    FILTRO_SHOPPING: '//thead/tr//th[4]',
    FILTRO_SITUACAO: '//thead/tr//th[5]',
    FILTRO_OPTIONS: '//thead/tr//th[6]',
    BTN_ADICIONAR_USUARIO: '//div[@class="container"]/div[@data-testid="card"]/div[1]/div[2]/div[1]//button[1]/span/..',
    BTN_FILTRAR: '//div[@class="container"]/div[@data-testid="card"]/div[1]/div[2]/div[1]//button[2]/span',
    INPUT_BUSCAR: '//div[@class="container"]/div/div[1]/div[2]/div[1]//input[@id="search-user"]',

    //ASSERTIVA MODAL DE SUCESSO
    MSG_SUCESSO: '//div[@class="styles__GlobalStyleDiv-sc-1a1upx4-0 khuxeo modal"]//h1',
    MODAL_SUCESSO: '//div[@class="styles__GlobalStyleDiv-sc-1a1upx4-0 khuxeo modal"]',


    //ASSERTIVAS FALHAS
    SENHAS_DIFERENTES: '//input[@id="user-confirm-password"]/../div/span[contains(.,"Senhas diferentes")]',
    PREENCHIMENTO_OBRIGATORIO1: '//input[@id="user-password"]/../div/span',
    PREENCHIMENTO_OBRIGATORIO2: '//input[@id="user-confirm-password"]/../div/span'
}