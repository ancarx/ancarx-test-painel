export const usuarios_alterar_ele = {
    CAMPO_BUSCA: '//div[@class="container"]/div/div[1]/div[2]/div[1]//input[@id="search-user"]',
    USER: '//tbody/tr/td/span[contains(.,"Cypress USER")]',
    USER_EDIT: '//tbody/tr/td/span[contains(.,"Cypress USER")]/../..//button[@data-testid="button"]',
    USER_EDIT_NAME: '#user-name',
    BTN_ALTERAR: '//button[@type="submit"]',
    MODAL_CONFIRMACAO: '//div[@data-testid="modal"]/div[2]',
    MSG_CONFIRMACAO: '//div[@data-testid="modal"]/div[2]//p',
    USER_PASSWORD: "#user-password",
    USER_CONFIRM_PASSWORD: '#user-confirm-password',
}