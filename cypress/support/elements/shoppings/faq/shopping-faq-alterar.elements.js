export const shoppings_faq_alt = {
    PAGE_BTN: length => `//div[@class="container"]/form/div/div[2]/button[${length}]`,
    BTN_EDITAR_RESPOSTA: '//button/span[contains(.,"Editar resposta")]/..',
    TITULO_PAG: '//h1',
    INP_CATEGORIA: '#category',
    INP_CATEGORIA_OPTION: '//div[@id="category"]/div/div/ul/li[1]',
    INP_PERGUNTA: '#question',
    INP_RESPOSTA_SITE: '#answer',
    INP_RESPOSTA_BOT: '#short-answer',
    MSG_MANUAL: '//div[@class="container"]/form/div/div[1]/div[2]/div[2]/div/span',

    //assertiva de sucesso
    MODAL: '//div[@data-testid="modal"]/div[2]',
    MSG_SUCESSO: '//div[@data-testid="modal"]/div[2]/div/div//p',
    BTN_MODAL: '//div[@data-testid="modal"]/div[2]/div/div/button',

    //assertiva falha
    MSG_FALHA: '//span[contains(.,"preenchimento obrigatório")]',

    //assertiva desistencia
    MSG_DESISTENCIA: '//div[@data-testid="modal"]/div[2]/div/div/h1',
    BTN_DESISTENCIA: length => `//div[@data-testid="modal"]/div[2]/div/div/div/button[${length}]`
}