export const shopping_faq_visu = {
    BTN_VER_DETALHES: '//tbody/tr[1]/td[4]//button[3]',
    
    //pegar valores
    VALORES_FAQ: length => `//tbody/tr[1]/td[${length}]//span`,

    //ver valores no detalhe
    VALORES_DETALHE: length => `//div[@class="container"]/div/div[2]/div[${length}]/span[2]`,
    
    //assertiva página
    TITULO_PAG: '//h1',
    BTN_EDITAR_RESPOSTA: '//button/span[contains(.,"Editar resposta")]/..',
    FILTRO_DETALHES: length => `//div[@class="container"]/div/div[2]/div[${length}]/span[1]`
}