export const shopping_faq_criar = {
    PAGE_BTN: length => `//div[@class="container"]/form/div/div[2]/button[${length}]`,
    BTN_ADICIONAR_RESPOSTA: '//div[@class="container"]/div/div/div[2]/div/button[contains(.,"Adicionar resposta")]',
    TITULO_PAG: '//h1',
    INP_CATEGORIA: '#category',
    INP_CATEGORIA_OPTION: '//div[@id="category"]/div/div/ul/li[1]',
    INP_PERGUNTA: '#question',
    INP_RESPOSTA_SITE: '#answer',
    INP_RESPOSTA_BOT: '#short-answer',

    BTN_ADICIONAR: '//button[contains(.,"Adicionar")]',

    MODAL: '//div[@data-testid="modal"]/div[2]',
    MSG_SUCESSO: '//div[@data-testid="modal"]/div[2]/div/div//p',
    BTN_MODAL: '//div[@data-testid="modal"]/div[2]/div/div/button',
}