export const shopping_faq_filtrar = {
    //filtrar
    BTN_FILTRAR: '//div[@class="container"]/div[1]/div[1]/div[2]//button[2]',
    MODAL: '//div[@data-testid="modal"]/div[2]',
    TITULO_MODAL: '//div[@data-testid="modal"]/div[2]/div[1]/h1',
    INP_CATEGORIA: '#category',
    INP_CATEGORIA_OPTION: '//div[@data-testid="modal"]/div[2]//ul/li[1]',
    BTN_MODAL: length => `//div[@data-testid="modal"]/div[2]/div[2]/div/div[2]/button[${length}]`,

    //assertivas pós filtro || busca
    ASSRTV_CATEGORIA: '//tbody/tr/td[1]/div/span',
    ASSRTV_PERGUNTA: '//tbody/tr/td[2]/div/span',
    ASSRTV_RESPOSTA: '//tbody/tr/td[3]/div/span',


    //buscar
    INP_BUSCAR: '#search-user'
}