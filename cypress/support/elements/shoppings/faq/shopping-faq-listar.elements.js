export const shopping_faq_listar = {
    BTN_FAQ: '//tr[1]/td[3]//button[2]',
    TITULO_PAG: '//h1',
    FILTRO_CATEGORIA: '//thead/tr/th[1]',
    FILTRO_PERGUNTA: '//thead/tr/th[2]',
    FILTRO_RESPOSTA: '//thead/tr/th[3]',
    BTN_ADICIONAR_RESPOSTA: '//div[@class="container"]/div//div[1]//button[contains(.,"Adicionar resposta")]',
    LISTA_PAG: length => `//tbody/tr[${length}]/td//span`
}