export const shopping_editar_piso_alt_ele = {
    BTN_EDITAR_PISO: '//div[@data-testid="modal"]/div[2]//tbody/tr[1]/td[3]//button[1]',
    NOME_PISO: '#name-floor-service',
    BTN_ALTERAR: '//button[contains(.,"Alterar")]',
    MODAL: '//div[@data-testid="modal"]/div[2]',
    BTN_MODAL: '//span[contains(.,"Voltar para a Lista de Shoppings")]'
}