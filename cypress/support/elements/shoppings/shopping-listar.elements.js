export const shoppings_ele = {
    TITULO_PAG: '//div[@data-testid="card"]//h1',
    FILTRO: '#filter-shopping',
    FILTRO_NOME: '//table[@data-testid="table"]/thead/tr/th[1]',
    FILTRO_SIGLA: '//table[@data-testid="table"]/thead/tr/th[2]',
    NOME_SHOPPING: length => `//table[@data-testid="table"]/tbody/tr${length}/td[1]`,
    SIGLA_SHOPPING: length => `//table[@data-testid="table"]/tbody/tr${length}/td[2]`,
    SERVICOS_SHOPPING: '//tbody/tr[1]/td[3]//button[1]',
    DETALHES_SHOPPING: '//table[@data-testid="table"]/tbody/tr[1]/td[3]//button[1]',
    EDITAR_SHOPPING: '//table[@data-testid="table"]/tbody/tr[1]/td[3]//button[2]'
}
