export const shopping_servicos_alt = {
    BTN_ALTERAR: '//tr[1]/td[2]//button[1]',
    TITULO_PAGINA: '//h1',
    PISO_SHOPPING: '#floor',
    PISO_SHOPPING_OPTIONS: '//div[@id="floor"]/div/div/ul/li[1]/div/div',
    COMPLEMENTO_SHOPPING: '#complement',
    TIPO_URL: '#type-url',
    TIPO_URL_OPTION: '//div[@id="type-url"]/div/div/ul/li[1]',
    URL: '#url',
    TELEFONE: '#phoneNumber',
    EMAIL: '#email',
    DESCRICAO_APP: '#description',
    DESCRICAO_BOT_1: '#short-description1',
    DESCRICAO_BOT_2: '#short-description2',
    HORARIO_FUNCIONAMENTO_SESSION: '//div[@class="container"]/form/div/div[2]',
    HORARIO_FUNCIONAMENTO: '#opening-hours',
    BTN_PAGE:btn => `//button[contains(.,"${btn}")]`,

    //modal
    MODAL: '//div[@data-testid="modal"]/div[2]',
    MODAL_MSG: '//div[@data-testid="modal"]/div[2]//p',
    MODAL_BTN: '//div[@data-testid="modal"]/div[2]//button'
}