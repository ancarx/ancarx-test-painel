export const shopping_servicos_ele = {
    NOME_SHOPPING: '//tbody/tr[1]/td[1]', //TITULO FORA DA PÁGINA DE SERVICOS
    BTN_SERVICOS: '//tbody/tr[1]/td[3]//button[1]',
    DETALHES_SERVICOS: (length1, length2) =>`//div[@class="container"]/div/div/div[2]/div${length1}/span${length2}`,
    
    //DENTRO DA PÁGINA DE SERVIÇOS
    NOME_SHOPPING_EM_SERVICOS: '//div[@class="container"]/div/div/div[1]/h1', //TITULO DENTRO DA PÁGINA DE SERVICOS
    EDITAR_ORDENACAO: '//div[@class="container"]/div/div/div[2]//span[contains(.,"Editar ordenação")]/..',
    FILTRO_BUSCAR: '#filter-shopping',
    COLUNA_NOME: '//div[@class="container"]/div/div[2]/table/thead/tr/th[1]',
    COLUNA_SIGLA: '//div[@class="container"]/div/div[2]/table/tbody/tr/td/div/span[2]',
    BTN_ACTIONS: '//div[@class="container"]/div/div[2]/table/tbody/tr/td[2]/div/button/span',
    NOME_SERVICO: length => `//tbody/tr[${length}]/td[1]//span[2]`,
    IMG_ERROR: '//img[@alt="impressed"]',
    BTN_DETALHES: '//div[@class="container"]/div/div[2]/table/tbody/tr[1]/td[2]/div/button[2]/span',

    //DENTRO DA PÁGINA DE DETALHES DO SERVIÇO
    BREADCRUMBS: '//a[@href="/shopping/SNA/service?shoppingName=Shopping%20Nova%20America"]',

    //EDITAR ORDENAÇÃO
    SERVICO: length => `//tbody/tr[${length}]/td[1]`

}