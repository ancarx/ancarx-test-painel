export const banners_add_ele = {
    BTN_ADICIONAR:'//div[@class="container"]/div/div/div[1]/div[2]//button[2]',
    MODAL: '//div[@data-testid="modal"]/div[2]',
    MODAL_FINAL: '//div[@data-testid="modal"][2]/div[2]',
    LINK_SELECT: '//div[@id="link"]',
    LINK_OPTIONS: options => `//div[@id="link"]//ul/li[contains(.,"${options}")]`,
    CARREGAR_IMG: '//span[contains(.,"Carregar imagem")]',
    DESCRICAO_IMG: '//input[@id="banner-img"]/../div[2]/span[1]',
    GABARITO_IMG: '//input[@id="banner-img"]/../div[2]/span[2]',
    BTN_MODAL: btn => `//div[@data-testid="modal"]/div[2]/div[2]//button[contains(.,"${btn}")]`,
    UPAR_IMAGEM: '#banner-img',

    //LINK INTERNO
    DESTINO_INTERNO: '//div[@id="destiny"]',
    DESTINO_INTERNO_OPTIONS: length => `//div[@id="destiny"]//ul/li[${length}]`,
    OBJETIVO: '//div[@id="objective"]',
    OBJETIVO_OPTIONS:length => `//div[@id="objective"]//ul/li[${length}]`,    

    //LINK EXTERNO
    DESTINO: '//input[@id="destiny"]',

    //CENÁRIO DE SUCESSO
    MSG_MODAL_SUCESSO: '//div[@data-testid="modal"][2]/div[2]/div//p',
    BTN_FINAL: btn_txt =>`//div[@data-testid="modal"][2]/div[2]//button[contains(.,"${btn_txt}")]`,

    //MSG DE FRACASSO 
    MSG_MODAL_DESISTENCIA: '//div[@data-testid="modal"][2]/div[2]/div//h1'
}