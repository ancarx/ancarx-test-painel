export const banners_ord_ele = {
    TITULO_PAG: '//div[@class="container"]/div/div/div[1]/div[1]/h1',
    BTN_ADICIONAR: '//div[@class="container"]/div/div/div[1]/div[2]/button[contains(.,"Adicionar banner")]',
    BTN_EDITAR_ORD: '//div[@class="container"]/div/div/div[1]/div[2]/button[contains(.,"Editar ordenação")]',
    FILTROS_PAG: '//thead//th',
    REGISTROS_PAG: '//tbody/tr',
    POSICAO_SELECT: '//tbody/tr[1]/td//div[@id="select-position-service"]',
    POSICAO_OPTIONS: '//tbody/tr[1]/td//ul/li[contains(.,"2")]',

    //CENÁRIO SUCESSO
    SALVAR_ORD: '//div[@class="container"]/div/div/div[3]/button[contains(.,"Salvar")]',
    CANCELAR_ORD: '//div[@class="container"]/div/div/div[3]/button[contains(.,"Cancelar")]',
    MSG_SUCESSO_ORD: '//div[@data-testid="modal"]/div[2]//p',
    BTN_SUCESSO_ORD: '//div[@data-testid="modal"]/div[2]//button',

    //CENÁRIO DE DESISTÊNCIA

    //CENÁRIO DE EXCLUSÃO
    EXCLUIR_BANNER: '//tbody/tr[1]/td[5]//button[1]',
    MSG_MODAL: msg => `//div[@data-testid="modal"]/div[2]//h1[contains(.,"${msg}")]`,
    BTN_MODAL: btn => `//div[@data-testid="modal"]/div[2]//button[contains(.,"${btn}")]`,
    VOLTAR_PAGINA: '//div[@data-testid="modal"]/div[2]//button[contains(.,"Voltar para página de banners")]'
}