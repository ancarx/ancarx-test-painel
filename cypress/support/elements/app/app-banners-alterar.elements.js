export const banners_alterar_ele = {
    BTN_ALTERAR:'//tbody/tr[1]/td[5]//button[2]',
    MODAL: '//div[@data-testid="modal"]/div[2]',
    MODAL_FINAL: '//div[@data-testid="modal"][2]/div[2]',
    LINK_SELECT: '//div[@id="link"]',
    LINK_OPTIONS: options => `//div[@id="link"]//ul/li[contains(.,"${options}")]`,
    CARREGAR_IMG: '//span[contains(.,"Carregar imagem")]',
    DESCRICAO_IMG: '//input[@id="banner-img"]/../div[2]/span[1]',
    GABARITO_IMG: '//input[@id="banner-img"]/../div[2]/span[2]',
    BTN_MODAL: btn => `//div[@data-testid="modal"]/div[2]/div[2]//button[contains(.,"${btn}")]`,
    UPAR_IMAGEM: '#banner-img',

    MSG_MODAL_SUCESSO: '//div[@data-testid="modal"][2]/div[2]/div//p',
    BTN_FINAL: btn_txt =>`//div[@data-testid="modal"][2]/div[2]//button[contains(.,"${btn_txt}")]`,
    //LINK EXTERNO
    DESTINO: '//input[@id="destiny"]',

    //LINK INTERNO
    DESTINO_INTERNO: '//div[@id="destiny"]',
    DESTINO_INTERNO_OPTIONS: length => `//div[@id="destiny"]//ul/li[${length}]`,
    OBJETIVO: '//div[@id="objective"]',
    OBJETIVO_OPTIONS: length => `//div[@id="objective"]//ul/li[${length}]`,

    //CONFIRMAÇÃO SE FOI ALTERADO OU NÃO
    ASSRTV_ALTERACAO: '//table[@data-testid="table"]/tbody/tr[1]/td[2]/div'
}