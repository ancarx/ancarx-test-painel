export const pesquisas_criar_ele = {
    TITULO_PAG: '//h1[contains(.,"Criar Pesquisa")]',
    NOME_PESQUISA: '#survey-name',
    URL_PESQUISA: '#survey-url',
    DESCRICAO_PESQUISA: '#survey-description',
    DATA_INICIAL_PESQUISA: '#start-date',
    DATA_FINAL_PESQUISA: '#end-date',
    BTN_CRIAR_PESQUISA: '//div[@class="container"]/div/div[1]/div[2]/button[contains(.,"Criar pesquisa")]',
    BTN_PAGE: btn => `//form/div/div[2]/button[contains(.,"${btn}")]`,
    MODAL: '//div[@data-testid="modal"]/div[2]/div',
    MODAL_MSG: msg =>`//div[@data-testid="modal"]/div[2]/div/div//p[contains(.,"${msg}")]`,
    MODAL_BTN: '//div[@data-testid="modal"]/div[2]/div/div/button[1]',
    MODAL_MSG_CANCELAR: '//div[@data-testid="modal"]/div[2]/div/div/p',
    MODAL_TITULO_CANCELAR: '//div[@data-testid="modal"]/div[2]/div/div/h2[contains(.,"Cancelar pesquisa?")]',
    MODAL_BTN_CANCELAR: length => `//div[@data-testid="modal"]/div[2]/div/div/div/button[${length}]`
}