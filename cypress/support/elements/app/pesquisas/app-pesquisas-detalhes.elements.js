export const pesquisa_detalhes_ele = {
    surveyDATA: length => `//tbody/tr[1]/td[${length}]//span`,
    BTN_DETALHES: '//tbody/tr[1]/td[6]//button',
    pesquisaDETALHES: length => `//div[@class="container"]/div/div/div[2]/div[${length}]/span[2]`,
    urlDETALHES: '//div[@class="container"]/div/div/div[2]/div[3]/a'
}