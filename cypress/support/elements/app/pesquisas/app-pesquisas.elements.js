export const pesquisas_ele = {
    TITULO_PAG: '//h1',
    BTN_CRIAR: '//button[contains(.,"Criar pesquisa")]',
    COLUNA_NOME: '//thead/tr/th[1]',
    COLUNA_URL: '//thead/tr/th[2]',
    COLUNA_DATA_INICIAL: '//thead/tr/th[3]',
    COLUNA_DATA_FINAL: '//thead/tr/th[4]',
    COLUNA_STATUS: '//thead/tr/th[5]',
    REGISTROS: '//tbody/tr[1]',
    REGISTROS_NOME: '//tbody/tr[1]/td[1]',
    REGISTROS_URL: '//tbody/tr[1]/td[2]',
    REGISTROS_DATA_INICIAL: '//tbody/tr[1]/td[3]',
    REGISTROS_DATA_FINAL: '//tbody/tr[1]/td[4]',
    REGISTROS_STATUS: '//tbody/tr[1]/td[5]'
}