export const recuperar_conta_ele = {
    TITULO_PAG: '//div[@class="container"]/div/div/div[1]//h1',
    INP_CPF: '#cpf',
    BTN_CONSULTAR: '//div[@class="container"]/div//form/div/div[2]//button',
    
    MODAL: '//div[@data-testid="modal"]/div[2]',
    MODAL_MSG_ERROR: '//div[@data-testid="modal"]/div[2]//p',   
    ERRO_MSG: '//input[@id="cpf"]/../div/span',

    INP_EMAIL:'#email',
    MODAL_MSG: '//div[@data-testid="modal"]/div[2]/div[1]/h1',
    
    FILTRO_CONTA: length =>`//div[@class="container"]//div[@data-testid="card"]/div[2]/ul/li[${length}]/span[1]`,
    DADOS_CONTA: length => `//div[@class="container"]//div[@data-testid="card"]/div[2]/ul/li[${length}]/span[2]`,
    
    BTN_NOVA_CONSULTA: '//div[@data-testid="modal"]/div[2]//button[contains(.,"Nova consulta")]',
    BTN: btn =>`//div[@class="container"]//div[@data-testid="card"]/div[2]/div/button[contains(.,"${btn}")]`,
    BTN_MODAL: btn => `//div[@data-testid="modal"]/div[2]/div[2]//form/button[contains(.,"${btn}")]`,

    MSG_SUCESSO: '//div[@data-testid="modal"][2]//p',
}