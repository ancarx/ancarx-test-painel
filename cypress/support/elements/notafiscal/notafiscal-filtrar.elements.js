export const Nf_filtro_ele = {
    VISUALIZAR_MODAL: '//div[@data-testid="modal"]/div[2]',
    TITULO_MODAL: '//div[@data-testid="modal"]/div[2]/div[1]/h1',
/*     DATA_INICIAL: '#date-send-start',
    DATA_FINAL: '#date-send-end', */
    STATUS: '#status-receipt',
    STATUS_SELECT: '//div[@id="status-receipt"]/div/div/ul/li[contains(.,"Reprovada")]',
    SHOPPING: '#shoppings',
    SHOPPING_SELECT: '//div[@id="shoppings"]/div/div/ul/li/div/div/input[@id="SNA18"]', //seleciona o 
    ASSRTV_NOTA_STATUS: '//tbody/tr/td[5]/span',
    ASSRTV_NOTA_SHOPPING: '//tbody/tr/td[2]//span[1]',
    BTN_FILTRAR_MODAL: '//form/div[2]/button[2]/span[contains(.,"Filtrar")]',
    BTN_LIMPAR_FILTRO: '//form/div[2]/button[1]/span[contains(.,"Limpar filtro")]',
}