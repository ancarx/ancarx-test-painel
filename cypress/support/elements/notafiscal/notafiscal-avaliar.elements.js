export const Nf_ele = {
    BTN_ACESSA_NF: '//a[@href="/receipt"]//span[contains(.,"Nota Fiscal")]',
    ASSRTV_TITULO: '//div[@class="container"]/div/div[1]/div[1]//h1',
    BTN_AVALIAR_NOTA: '//div[@class="container"]/div/div[1]/div[2]/div[1]//span[contains(.,"Avaliar nota")]/..',
    BTN_FILTRAR: '//div[@class="container"]/div/div[1]/div[2]/div[1]//span[contains(.,"Filtrar")]/..',

    //Liberar notas
    ABA_EM_AVALIACAO: '//div[@class="container"]/div/div[2]/div/div[contains(.,"Em avaliação")]',
    LIBERAR_NOTA: length => `//tbody/tr[${length}]/td[10]/div/button[1]`,

    //Avaliar Nota - Preencher campos
    CONTAINER: '//div[@class="container"]//div[@data-testid="card"]/div[2]',
    NOT_FOUND: '//div[@data-testid="card"]/div[@data-testid="notfound"]//p',
    LOJA: '#store',
    LOJA_SELECT: '//input[@id="store"]/../../div[3]/ul/li[1]',
    CNPJ: '#cnpj',
    DATA_COMPRA: '#buy-date',
    DATA_COMPRA_HOJE: '//span[contains(.,"Hoje")]/..',
    CHECKBOX_DATA: '#todayd-ate',
    VALOR: '#value',
    FORMA_PAGAMENTO: '#payment-type',
    FORMA_PAGAMENTO_OPTIONS:length => `//div[@id="payment-type"]/div/div/ul/li${length}`,
    NF: '#invoice',
    CHECKBOX_APROV_PARCIAL: '#partial-approval',
    BTN: btn => `//span[contains(.,"${btn}")]/..`, //botão usado para Aprovar, Reprovar, Adiar e Finalizar avaliação
    
    //REPROVAÇÃO
    MOTIVO_REPROVACAO: '#reason-failure',
    MOTIVO_REPROVACAO_OPTIONS: length => `//div[@id="reason-failure"]/div/div/ul/li${length}`,

    //ADIAR 
    TEMPO_ADIAR: '#postpone-evaluation',
    TEMPO_ADIAR_OPTIONS: length => `//div[@id="postpone-evaluation"]/div/div/ul/li${length}`,
    
    //MODAL DE ADIAMENTO
    MODAL: '//div[@data-testid="modal"]/div[2]',
    MODAL_MSG: '//div[@data-testid="modal"]/div[2]/div/div/h1',
    MODAL_BTN: btn => `//div[@data-testid="modal"]/div[2]/div/div/div[@class="btn-modal-actions"]/button['${btn}']`

}