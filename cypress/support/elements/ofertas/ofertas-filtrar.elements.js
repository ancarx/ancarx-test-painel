export const filtro_off_ele = {
    ACESSA_PAGINA: '//a[@href="/receipt"]//span[contains(.,"Ofertas")]',
    BTN_FILTRAR: '//div[@class="container"]/div/div[1]/div[2]/div[1]//span[contains(.,"Filtrar")]/..',
    VISU_MODAL:'//div[@data-testid="modal"]/div[2]',
    FILTRO_DT_INICIAL: '#start-date',
    FILTRO_DT_FINAL: '#end-date',
    FILTRO_TITULO: '#title',
/*     FILTRO_CATEGORIA: '#category',
    FILTRO_CAT_OPTION: '//div[@id="category"]/div/div/ul/li[contains(.,"Óticas")]',
    FILTRO_DESTAQUE: '#hightlight',
    FILTRO_DEST_OPTION: '//div[@id="hightlight"]/div/div/ul/li[contains(.,"Sim")]', */
    FILTRO_STATUS: '#status',
    FILTRO_STAT_OPTION: length => `//div[@id="status"]/div/div/ul/li[contains(.,"${length}")]`,
    FILTRO_BOTAO_SUBMIT: '//form/div[2]/button[2]/span[contains(.,"Filtrar")]',
    FILTRO_BOTAO_LIMPAR: '//form/div[2]/button[1]/span[contains(.,"Limpar Filtro")]',
    ASSERTIVA_PRODUTO: '//tbody/tr/td[8]/span//span[1]',
}