export const ofertas_ele = {
    BTN_ACESSA_OFERTAS: '//a[@href="/promotion"]//span[contains(.,"Ofertas")]',
    ASSRTV_TITULO_PAG: '//div[@class="container"]/div/div[1]/div[1]//h1',
    BTN_CRIAR_OFERTA: '//div[@class="container"]/div/div[1]/div[2]/div[1]//span[contains(.,"Criar Oferta")]/..',

    //NA HORA DE CRIAR OFERTA
    BTN_CARREGAR_IMG: '#offer-img',
    TITULO: '#offer-title',
    DESTAQUE: op => `${op}`, //#highlight-not #highlight-yes
    EXCLUSIVA: op => `#exclusive-not`, //#exclusive-not #exclusive-yes
    CATEGORIA: '#category',
    CAT_SELECT: '//div[@id="category"]//li[contains(.,"Casa")]',
    DESCRICAO: '#description',
    CAMPANHA: '#campaign',
    BTN_LOJA: '#store',
    LOJA_SELECT: '//input[@id="store"]/../..//li[contains(.,"Shopping")]',
    BTN_TIPO: '#type',
    TIPO_SELECT: '//div[@id="type"]//li[contains(.,"De | Por")]',
    DE_QUANTO: '#from',
    POR_QUANTO: '#until',
    DATA_INICIAL: '#start-date',
    HORARIO_INICIAL: '#start-time',
    DATA_FINAL: '#end-date',
    HORARIO_FINAL: '#end-time',
    DT_RESGATE_CUPOM: '#rescue-coupon-date',
    BTN_LIMITE_CUPOM: '#switch',
    LIMITE_EMISSOES: '//label[contains(.,"Limite de emissões")]/..//input[@id="limite de emissões"]',
    LIMITE_RESGATES: '//label[contains(.,"Limite do resgate")]/..//input[@id="Limite do resgate"]',
    LIMITE_CLIENTE: '//label[contains(.,"Limite por cliente")]/..//input[@id="Limite por cliente"]',
    BTN_SALVAR_RASCUNHO: '//button[contains(.,"Salvar rascunho")]',
    MODAL: '//div[@data-testid="modal"]/div[2]',
    ASTV_RASCUNHO_SUCESSO: '//div[@data-testid="modal"]/div[2]/div/div//p',
    BTN_VOLTAR_PARA_LISTA: '//button[contains(.,"Voltar para lista ofertas")]',

    //faz assertiva
    
    EDITAR_RASCUNHO: title => `//tbody/tr/td[2]//div/div[@title="${title}"]/../../../td[10]//button[2]`,
    BTN_PUBLICAR: '//button[contains(.,"Salvar e publicar")]',
    ASTV_PUBLICACAO_SUCESSO: '//div[@data-testid="modal"]/div[2]/div/div//p'
}