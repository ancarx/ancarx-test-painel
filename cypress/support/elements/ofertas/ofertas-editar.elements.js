export const ofertas_editar_ele = { 
    BTN_EDITAR_OFERTAS: '//tbody/tr[1]/td[10]/div/button[2]',
    BTN_SALVAR_EDICAO: '//button[contains(.,"Salvar edição")]',
    CATEGORIA: '//div[@id="category"]/div',  //essa div fica disabled, a do ID não fica disabled
    CAMPANHA: '//div[@id="campaign"]/div',
    BTN_TIPO: '//div[@id="type"]/div',
}