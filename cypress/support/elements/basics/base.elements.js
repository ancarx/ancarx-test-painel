export const Base = {
    AMB_HOM: 'https://management-panel-dot-ancarx-hom.rj.r.appspot.com/login',
    AMB_DEV: 'https://management-panel-dot-ancarx-dev.rj.r.appspot.com/login',
    //Selecionar Shopping SNA
    SHOPPING_CODE: '//nav/div/div',
    SHOPPING_SELECT_SNA: '//nav/div/div/div//li[contains(.,"SNA")]',
};