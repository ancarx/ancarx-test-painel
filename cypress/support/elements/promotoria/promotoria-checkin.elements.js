export const promotoria_ele = {
    // ASSERTIVA
    TITULO_PAG: '//div[@class="container"]/div[@data-testid="card"]//h1',
    THUMB_EVENTO: '//div[@class="container"]/div[1]/div[2]/div/div/div[@data-testid="card"]/div[1]//img[@alt="preview"]',
    TITULO_EVENTO: '//div[@class="container"]/div[1]/div[2]/div/div/div[@data-testid="card"]/p',

    //
    ESCOLHER_EVENTO: '//div[@class="styles__GlobalStyleDiv-sc-1a1upx4-0 ihfvYr grid"]/div//p[contains(.,"Automação - et")]/../button',
    ESCOLHER_SESSAO: '//h2[contains(.,"AUT. Sessão Cypress")]',
    MODAL_CODIGO: '//div[@data-testid="modal"]/div[2]',
    TITULO_MODAL: '//div[@data-testid="modal"]/div[2]/div[1]/h1',
    CODIGO_EVENTO: '#code', 
    BTN_VALIDAR: '//button[@type="submit"]',
    BTN_CANCELAR: '//button[@type="button"]',

    THUMB_INGRESSO: '//form/div[1]/div[1]//img',
    NOME_EVENTO_INGRESSO: '//form/div[1]/div[2]/h2',
    DADOS_INGRESSO: '//form/div[1]/div[2]/p',
    TITULAR_INGRESSO: '//form/div[2]//span[contains(.,"Nome:")]',
    ACOMPANHANTES_INGRESSO: '//form/div[2]//div[2]/ul/li/div/span',
    CLASSIFICACAO_INGRESSO: '//form/div[2]//div[1]/div/span/span[contains(.,"L")]',
    CHECKBOX_ACESSO: '//form/div[2]//div[2]/ul/li/div/div',
    BTN_LIBERAR_ACESSO: '//form/button/span[contains(.,"Liberar acesso")]/..',
    MODAL_CONFIRMACAO: '//div[@data-testid="modal"]/div[2]',
    MSG_CONFIRMACAO: '//div[@data-testid="modal"]/div[2]//p',
    BTN_NOVA_CONSULTA: '//div[@data-testid="modal"]/div[2]//button',


    //Cenários Falhos
    MSG_ERRO_CODIGO: '//input[@id="code"]/../div/span'
}