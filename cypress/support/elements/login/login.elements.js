export const Logar = {
    EMAIL: '#user-email',
    SENHA: '#user-password',
    MOSTRAR_SENHA: '//input[@id="user-password"]/../../div[2]/div',
    BTN_ENTRAR: '//form//span[contains(.,"Entrar")]/..',
    MSG_BOAS_VINDAS: 'h1',
    MSG_ERRO: '//form/div[3]//span'
};