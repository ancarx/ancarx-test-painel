import {filtro_off_ele } from "../../elements/ofertas/ofertas-filtrar.elements";

export class Ofertas_F{

    static acessar_filtro(){
        cy.wait(1000)
        cy.xpath(filtro_off_ele.BTN_FILTRAR).should('not.be.disabled', {timeout:10000}).click();
    }
    static visualizar_modal(){
        cy.xpath(filtro_off_ele.VISU_MODAL, {timeout: 6000}).should('be.visible')
    }
    static botão_limpar_existe(){
        cy.xpath(filtro_off_ele.FILTRO_BOTAO_LIMPAR).should('be.exist')
    }
    static preencher_filtros(){
/*         cy.get(filtro_off_ele.FILTRO_DT_INICIAL).type('29112021');
        cy.get(filtro_off_ele.FILTRO_DT_FINAL).type('01012022');
        cy.get(filtro_off_ele.FILTRO_CATEGORIA).click();
        cy.xpath(filtro_off_ele.FILTRO_CAT_OPTION).click();
        cy.get(filtro_off_ele.FILTRO_DESTAQUE).click();
        cy.xpath(filtro_off_ele.FILTRO_DEST_OPTION).click(); */
        cy.get(filtro_off_ele.FILTRO_STATUS).click();
        cy.xpath(filtro_off_ele.FILTRO_STAT_OPTION('Aprovada')).click();
    }
    static clicar_filtrar(){
        cy.xpath(filtro_off_ele.FILTRO_BOTAO_SUBMIT).click()
    }
    static faz_assertiva(){
        cy.xpath(filtro_off_ele.ASSERTIVA_PRODUTO, {timeout: 6000}).each((item) => {
            cy.wrap(item).should('contain.text', 'Aprovada');
        })
    }
}