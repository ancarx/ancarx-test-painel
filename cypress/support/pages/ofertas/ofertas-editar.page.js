import { ofertas_ele } from "../../elements/ofertas/ofertas-criar.elements";
import { ofertas_editar_ele } from "../../elements/ofertas/ofertas-editar.elements";
export class Ofertas_E {
    static acessar_edição_oferta(){
        cy.xpath(ofertas_editar_ele.BTN_EDITAR_OFERTAS).click()
    }
    static assrtv_pagina(){
        cy.wait(1500)
        cy.get(ofertas_ele.TITULO).should('be.disabled')
        cy.xpath(ofertas_editar_ele.CATEGORIA).should('have.attr', 'disabled')
        cy.get(ofertas_ele.DESCRICAO).should('be.disabled')
        cy.xpath(ofertas_editar_ele.CAMPANHA).should('have.attr', 'disabled')
        cy.get(ofertas_ele.BTN_LOJA).should('be.disabled')
        cy.xpath(ofertas_editar_ele.BTN_TIPO).should('have.attr', 'disabled')
        cy.get(ofertas_ele.DE_QUANTO).should('be.disabled')
        cy.get(ofertas_ele.POR_QUANTO).should('be.disabled')
        cy.get(ofertas_ele.DATA_INICIAL).should('be.disabled')
        cy.get(ofertas_ele.HORARIO_INICIAL).should('be.disabled')
        cy.get(ofertas_ele.DATA_FINAL).should('be.disabled')
        cy.get(ofertas_ele.HORARIO_FINAL).should('be.disabled')
        cy.get(ofertas_ele.DT_RESGATE_CUPOM).should('be.disabled')
        cy.xpath(ofertas_editar_ele.BTN_SALVAR_EDICAO).should('be.disabled')
    }
    static editar_oferta(){
        cy.get(ofertas_ele.DESTAQUE('#highlight-yes')).click();
        cy.get(ofertas_ele.EXCLUSIVA('#exclusive-yes')).click();
        cy.get(ofertas_ele.DESTAQUE('#highlight-not')).click();
        cy.get(ofertas_ele.EXCLUSIVA('#exclusive-not')).click();
    }
    static salvar_edicao(){
        cy.interceptAPI().as('loadSalvar')
        cy.xpath(ofertas_editar_ele.BTN_SALVAR_EDICAO).click()
        cy.wait('@loadSalvar')
    }
    static assrtv_modal(){
        cy.xpath(ofertas_ele.MODAL).should('be.visible')
        cy.xpath(ofertas_ele.ASTV_RASCUNHO_SUCESSO, {timeout: 7500}).should('contain','Edição salva com sucesso!')
        cy.xpath(ofertas_ele.BTN_VOLTAR_PARA_LISTA).should('be.visible')
    }
    static voltar_para_lista(){
        cy.xpath(ofertas_ele.BTN_VOLTAR_PARA_LISTA).click()
    }
}