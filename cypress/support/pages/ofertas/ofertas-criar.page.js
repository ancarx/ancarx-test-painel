import {ofertas_ele} from "../../elements/ofertas/ofertas-criar.elements";
import { filtro_off_ele } from "../../elements/ofertas/ofertas-filtrar.elements";
const fixtureImage = 'aj06.png'

const faker = require ('faker')
var loremWords = faker.lorem.words(4)
var loremSentences = faker.lorem.sentence()
export class Ofertas {
    static acessar_ofertas(){
        cy.xpath(ofertas_ele.BTN_ACESSA_OFERTAS).click()
    }
    static assertiva_pagina(){
        /* cy.server()
        cy.route('GET', 'https://api.ancar.com.br/hml/api-ancarx/api/v1/panel/promotions?page=1&newFeature=true').as('loadOferta')
        cy.route('GET', 'https://api.ancar.com.br/hml/api-ancarx/api/v1/campaigns?status=active').as('loadCampaigns')
        cy.wait('@loadOferta').its('status').should('eq', 200)
        cy.wait('@loadCampaigns').its('status').should('eq', 200) */
        cy.xpath(ofertas_ele.ASSRTV_TITULO_PAG).should('have.text', 'Lista de Ofertas')
    }
    static acessar_criar_ofertas(){
        cy.xpath(ofertas_ele.BTN_CRIAR_OFERTA, {timeout: 8500}).should('not.be.disabled').click();
    }
    static upar_imagem(){
        cy.wait(1000)
        cy.get(ofertas_ele.BTN_CARREGAR_IMG, {timeout: 6000}).attachFile(fixtureImage);
    }
    static preencher_campos(){
        cy.get(ofertas_ele.TITULO).type(`Automação - `+`${loremWords}`);
        cy.get(ofertas_ele.DESTAQUE('#highlight-not')).click();
        cy.get(ofertas_ele.EXCLUSIVA('#exclusive-not')).click();

        cy.get(ofertas_ele.CATEGORIA).click();
        cy.xpath(ofertas_ele.CAT_SELECT).click();

        cy.get(ofertas_ele.DESCRICAO).type('Garante 15% de desconto');

        cy.get(ofertas_ele.BTN_LOJA).click();
        cy.xpath(ofertas_ele.LOJA_SELECT).click();

        cy.get(ofertas_ele.BTN_TIPO).click();
        cy.xpath(ofertas_ele.TIPO_SELECT).click();

        cy.get(ofertas_ele.DE_QUANTO).type('59,90');
        cy.get(ofertas_ele.POR_QUANTO).type('29,90');
        cy.get(ofertas_ele.DATA_INICIAL).type('21012021');
        cy.get(ofertas_ele.HORARIO_INICIAL).type('1300');
        cy.get(ofertas_ele.DATA_FINAL).type('30012022');
        cy.get(ofertas_ele.HORARIO_FINAL).type('1300');
        cy.get(ofertas_ele.DT_RESGATE_CUPOM).type('30122021')
        cy.get(ofertas_ele.BTN_LIMITE_CUPOM).click();
        cy.xpath(ofertas_ele.LIMITE_EMISSOES).type('10');
        cy.xpath(ofertas_ele.LIMITE_RESGATES).type('10');
        cy.xpath(ofertas_ele.LIMITE_CLIENTE).type('1');
    }
    static salvar_rascunho(){
        cy.interceptAPI().as('loadOffers')
        cy.xpath(ofertas_ele.BTN_SALVAR_RASCUNHO).click();
        cy.wait('@loadOffers')
    }
    static assertiva_rascunho(){
        cy.xpath(ofertas_ele.MODAL).should('be.visible')
        cy.xpath(ofertas_ele.ASTV_RASCUNHO_SUCESSO, {timeout: 6500}).should('contain','Rascunho do cadastro salvo com sucesso!')
        cy.xpath(ofertas_ele.BTN_VOLTAR_PARA_LISTA).should('be.visible')
    }
    static voltar_para_lista(){
        cy.xpath(ofertas_ele.BTN_VOLTAR_PARA_LISTA).click();
    }
    static filtra_rascunho(){
        cy.xpath(filtro_off_ele.BTN_FILTRAR).should('not.be.disabled').click()
        cy.get(filtro_off_ele.FILTRO_TITULO).type(`Automação - `+`${loremWords}`)
        cy.xpath(filtro_off_ele.FILTRO_BOTAO_SUBMIT).click()
        cy.wait('@loadOffers')
    }
    static editar_rascunho(){
        cy.xpath(ofertas_ele.EDITAR_RASCUNHO(`Automação - `+`${loremWords}`)).click();
    }
    static publicar_oferta(){
        cy.interceptAPI().as('loadOffers')
        cy.xpath(ofertas_ele.BTN_PUBLICAR).click();
        cy.wait('@loadOffers')
    }
    static assertiva_publicacao(){
        cy.xpath(ofertas_ele.ASTV_PUBLICACAO_SUCESSO).should('contain', 'Cadastro salvo e publicado com sucesso');
    }
}