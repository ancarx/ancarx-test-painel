import { recuperar_conta_ele } from "../../elements/app/app-recuperar_conta.page"
const faker = require('faker');
let email = faker.internet.email()

export class Recuperar_conta{
    static assertiva_pag(){
        cy.xpath(recuperar_conta_ele.TITULO_PAG).should('contain', 'Recuperar conta')
        cy.get(recuperar_conta_ele.INP_CPF).should('be.visible')
        cy.xpath(recuperar_conta_ele.BTN_CONSULTAR).should('be.visible') 
    }
    static consultar_cpf(cpf){ //06570944045
        cy.interceptAPI().as('loadBanners')
        cy.get(recuperar_conta_ele.INP_CPF).type(cpf)
        cy.xpath(recuperar_conta_ele.BTN_CONSULTAR).click()
        cy.wait('@loadBanners')
    }
    static assrtv_cpf_não_encontrado(){
        cy.xpath(recuperar_conta_ele.MODAL).should('be.visible')
        cy.xpath(recuperar_conta_ele.MODAL_MSG_ERROR).should('contain','CPF não encontrado na base de cadastro!')
        cy.xpath(recuperar_conta_ele.BTN_NOVA_CONSULTA).should('be.visible')
    }
    static nova_consulta(){
        cy.xpath(recuperar_conta_ele.BTN_NOVA_CONSULTA).click()
        cy.xpath(recuperar_conta_ele.ERRO_MSG).should('be.visible')
    }
    static assrtv_consulta(){
        for(var i = 1; i < 7; ++i){
            var length = i
            cy.xpath(recuperar_conta_ele.FILTRO_CONTA(`${length}`)).should('be.visible')
            cy.xpath(recuperar_conta_ele.DADOS_CONTA(`${length}`)).then(item =>{
                cy.wrap(item).log(item.text())
            })
        }
        cy.xpath(recuperar_conta_ele.BTN('Cancelar')).should('be.visible')
        cy.xpath(recuperar_conta_ele.BTN('Recuperar acesso')).should('be.visible')
    }
    static recuperar_acesso(){
        cy.xpath(recuperar_conta_ele.BTN('Recuperar acesso')).click()
    }
    static assertiva_modal(){
        cy.xpath(recuperar_conta_ele.MODAL).should('be.visible')
        cy.xpath(recuperar_conta_ele.MODAL_MSG).should('contain', 'Recuperar acesso')
        cy.get(recuperar_conta_ele.INP_EMAIL).should('be.visible')
        cy.xpath(recuperar_conta_ele.BTN_MODAL('Enviar')).should('be.visible')
        cy.xpath(recuperar_conta_ele.BTN_MODAL('Cancelar')).should('be.visible')
    }
    static preencher_modal(){
        cy.get(recuperar_conta_ele.INP_EMAIL).type(`${email}`)
        cy.interceptAPI().as('loadBanners')
        cy.xpath(recuperar_conta_ele.BTN_MODAL('Enviar')).click()
        cy.wait('@loadBanners')
    }
    static assrtv_sucesso(){
        cy.xpath(recuperar_conta_ele.MSG_SUCESSO).should('contain', 'Mensagem com o link de recuperação de acesso enviada!')
        cy.xpath(recuperar_conta_ele.BTN_NOVA_CONSULTA).click()
    }
} 