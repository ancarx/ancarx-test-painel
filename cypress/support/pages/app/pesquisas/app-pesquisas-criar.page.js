import { pesquisas_criar_ele } from "../../elements/app/app-pesquisas-criar.elements";

const faker = require ('faker')
const dayjs = require('dayjs')
var loremWords = faker.lorem.words(3)
var loremSentences = faker.lorem.sentence()
export class Pesquisas_criar{
    static btn_criar_pesquisa(){
        cy.xpath(pesquisas_criar_ele.BTN_CRIAR_PESQUISA).should('not.be.disabled').click()
    }
    static assertiva_pagina(){
        cy.xpath(pesquisas_criar_ele.TITULO_PAG).should('be.visible')
        cy.get(pesquisas_criar_ele.NOME_PESQUISA).should('be.visible')
        cy.get(pesquisas_criar_ele.URL_PESQUISA).should('be.visible')
        cy.get(pesquisas_criar_ele.DESCRICAO_PESQUISA).should('be.visible')
        cy.get(pesquisas_criar_ele.DATA_INICIAL_PESQUISA).should('be.visible')
        cy.get(pesquisas_criar_ele.DATA_FINAL_PESQUISA).should('be.visible')
        cy.xpath(pesquisas_criar_ele.BTN_PAGE('Cancelar')).should('be.visible')
        cy.xpath(pesquisas_criar_ele.BTN_PAGE('Salvar rascunho')).should('be.disabled')
        cy.xpath(pesquisas_criar_ele.BTN_PAGE('Salvar e publicar')).should('be.disabled')
    }
    static preencher_pesquisa(){
        cy.get(pesquisas_criar_ele.NOME_PESQUISA).type('AUT - '+`${loremWords}`+' ?')
        cy.get(pesquisas_criar_ele.URL_PESQUISA).type('https://management-panel-dot-ancarx-hom.rj.r.appspot.com/')
        cy.get(pesquisas_criar_ele.DESCRICAO_PESQUISA).type(loremSentences)
        cy.get(pesquisas_criar_ele.DATA_INICIAL_PESQUISA).type(dayjs().format('DD/MM/YYYY'))
        cy.get(pesquisas_criar_ele.DATA_FINAL_PESQUISA).type(dayjs().add(1, 'day').format('DD/MM/YYYY'))
    }
    static btn_page(btn){
        cy.xpath(pesquisas_criar_ele.BTN_PAGE(btn)).should('be.not.disabled').click()
    }
    static assrtv_modal(msg){
        cy.xpath(pesquisas_criar_ele.MODAL).should('be.visible')
        cy.xpath(pesquisas_criar_ele.MODAL_MSG(msg)).should('have.text',msg)
    }
    static assrtv_modal_cancelar(){
        cy.xpath(pesquisas_criar_ele.MODAL).should('be.visible')
        cy.xpath(pesquisas_criar_ele.MODAL_TITULO_CANCELAR).should('be.visible')
        cy.xpath(pesquisas_criar_ele.MODAL_MSG_CANCELAR).should('be.visible')
        cy.xpath(pesquisas_criar_ele.MODAL_BTN_CANCELAR('1')).should('be.visible')
        cy.xpath(pesquisas_criar_ele.MODAL_BTN_CANCELAR('2')).should('be.visible')
    }
    static concluir_fluxo(){
        cy.xpath(pesquisas_criar_ele.MODAL_BTN).click()
    }
    static concluir_fluxo_cancelar(btn){
        cy.xpath(pesquisas_criar_ele.MODAL_BTN_CANCELAR(btn)).click()
    }
}