import {pesquisa_detalhes_ele} from "../../../elements/app/pesquisas/app-pesquisas-detalhes.elements"

export class Pesquisas_detalhes {
    static pega_valores_e_compara_com_detalhe() {
        describe('pega elementos primarios', function () {
            let name = null
            let url = null
            cy.xpath(pesquisa_detalhes_ele.surveyDATA('1')).then(($el) => (name = $el.text()))
                .then(() => {
                    cy.xpath(pesquisa_detalhes_ele.surveyDATA('2')).then(($el) => (url = $el.text()))
                        .then(() => {
                            cy.xpath(pesquisa_detalhes_ele.BTN_DETALHES).click();
                            cy.wait(500)
                            /* nome  */
                            cy.xpath(pesquisa_detalhes_ele.pesquisaDETALHES('1')).should('contain', name);
                            /* situacao  */
                            cy.xpath(pesquisa_detalhes_ele.urlDETALHES).should('contain', url);
                        })
                })
        })
    }
}