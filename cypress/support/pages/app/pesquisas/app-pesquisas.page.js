import {
    pesquisas_ele
} from "../../../elements/app/pesquisas/app-pesquisas.elements"

export class Pesquisas {
    static assertiva_pagina() {
        cy.xpath(pesquisas_ele.TITULO_PAG).should('have.text', 'Lista de pesquisas')
        cy.xpath(pesquisas_ele.BTN_CRIAR).should('be.visible')
        cy.xpath(pesquisas_ele.COLUNA_NOME).should('have.text', 'Nome da pesquisa')
        cy.xpath(pesquisas_ele.COLUNA_URL).should('have.text', 'URL')
        cy.xpath(pesquisas_ele.COLUNA_DATA_INICIAL).should('have.text', 'Data inicial')
        cy.xpath(pesquisas_ele.COLUNA_DATA_FINAL).should('have.text', 'Data final')
        cy.xpath(pesquisas_ele.COLUNA_STATUS).should('have.text', 'Status')
    }
    static assertiva_registros() {
        cy.xpath(pesquisas_ele.REGISTROS).should('be.visible')
        cy.xpath(pesquisas_ele.REGISTROS_NOME).should('be.visible')
        cy.xpath(pesquisas_ele.REGISTROS_URL).should('be.visible')
        cy.xpath(pesquisas_ele.REGISTROS_DATA_INICIAL).should('be.visible')
        cy.xpath(pesquisas_ele.REGISTROS_DATA_FINAL).should('be.visible')
        cy.xpath(pesquisas_ele.REGISTROS_STATUS).should('be.visible')
    }
    static btn_cancelar_pesquisa() {
        cy.xpath('//tbody/tr/td[contains(.,"Rascunho")]/../td[6]/div/button[1]').first().click()
    }
    static assrtv_modal_cancelar() {
        cy.xpath('//div[@data-testid="modal"]/div[2]').should('be.visible')
        cy.xpath('//div[@data-testid="modal"]/div[2]//h2').should('have.text', 'Cancelar pesquisa?')
        cy.xpath('//div[@data-testid="modal"]/div[2]//p').should('have.text', ' A pesquisa ficará inativa e não poderá ser editada, mas os dados não serão perdido.')
        cy.xpath('//div[@data-testid="modal"]/div[2]//div[@class="btn-modal-actions"]/button[1]').should('be.visible')
        cy.xpath('//div[@data-testid="modal"]/div[2]//div[@class="btn-modal-actions"]/button[2]').should('be.visible')
    }
    static cancelar_pesquisa() {
        cy.xpath('//div[@data-testid="modal"]/div[2]//div[@class="btn-modal-actions"]/button[2]').click()
    }
}