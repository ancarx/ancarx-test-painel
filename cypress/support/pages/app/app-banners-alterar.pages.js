import { banners_alterar_ele } from "../../elements/app/app-banners-alterar.elements";

export class Banners_alterar{
    static alterar_banner(){
        cy.wait(1000)
        cy.xpath(banners_alterar_ele.BTN_ALTERAR, {timeout: 6500}).click();
    }
    static assrtv_modal(){
        cy.xpath(banners_alterar_ele.MODAL, {timeout: 6500}).should('be.visible')
        cy.xpath(banners_alterar_ele.LINK_SELECT).should('be.visible')
        cy.xpath(banners_alterar_ele.CARREGAR_IMG).should('be.visible');
        cy.xpath(banners_alterar_ele.DESCRICAO_IMG).should('contain', 'Imagem 1.464x1.044 (JPG ou PNG)')
        cy.xpath(banners_alterar_ele.GABARITO_IMG).should('contain', 'Ver Gabarito')
        cy.xpath(banners_alterar_ele.BTN_MODAL('Cancelar')).should('be.visible');
        cy.xpath(banners_alterar_ele.BTN_MODAL('Salvar edição')).should('be.visible');
    }
    static selecionar_link(select){
        cy.xpath(banners_alterar_ele.LINK_SELECT).click()
        cy.xpath(banners_alterar_ele.LINK_OPTIONS(select)).click()
        cy.wait(1000)
    }
    static fluxo_externo(){
        cy.xpath(banners_alterar_ele.DESTINO)
                        .clear()
                        .type('https://management-panel-dot-ancarx-hom.rj.r.appspot.com/home')
    }
    static fluxo_interno(){
        cy.xpath(banners_alterar_ele.DESTINO_INTERNO).click()
        cy.xpath(banners_alterar_ele.DESTINO_INTERNO_OPTIONS('1'), {timeout: 6500}).click(); //opções de destino
        cy.wait(500)               
        cy.xpath(banners_alterar_ele.OBJETIVO).click()
        cy.xpath(banners_alterar_ele.OBJETIVO_OPTIONS('1')).click()
    }
    static salvar_edicao(){
        cy.interceptAPI().as('loadBanners1')
        cy.xpath(banners_alterar_ele.BTN_MODAL('Salvar edição')).should('be.not.disabled', {timeout: 8500}).click()
        cy.wait('@loadBanners1')
    }
    static assrtv_modal_final_sucesso(){
        cy.xpath(banners_alterar_ele.MODAL_FINAL).should('be.visible')
        cy.xpath(banners_alterar_ele.MSG_MODAL_SUCESSO).should('contain', 'Edição salva com sucesso!')
    }
    static completar_fluxo(){
        cy.xpath(banners_alterar_ele.BTN_FINAL('Voltar para página de banners')).click()
    }
    //ASSERTIVA SE FOI ALTERADO
    static assrtv_alteracao(text){
        cy.xpath(banners_alterar_ele.ASSRTV_ALTERACAO).should('have.text', text)
    }
}