import {
    banners_add_ele
} from "../../elements/app/app-banners-adicionar.elements"
const fixtureImage = 'jpg2.jpg'

export class Banners_adicionar {
    static delete_se_necessário() {
        cy.xpath('//div[@class="container"]/div/div/div[1]/div[2]//button[2]').then((button) => {
            if (button.is(':disabled')) {
                cy.xpath('//tbody/tr[5]/td[5]//button[1]').click()
                cy.xpath('//div[@data-testid="modal"]/div[2]//button[contains(.,"Sim, quero remover")]').click()
                cy.xpath('//div[@data-testid="modal"]/div[2]//button[contains(.,"Voltar para página de banners")]').click()
            } else {
                cy.log('É possível cadastrar')
            }
        })
    }
    static adicionar_banner() {
        cy.xpath(banners_add_ele.BTN_ADICIONAR, {timeout: 8500}).should('not.be.disabled').click()
    }
    static assrtv_modal() {
        cy.xpath(banners_add_ele.MODAL, {timeout: 7500}).should('be.visible')
        cy.xpath(banners_add_ele.LINK_SELECT).should('be.visible')
        cy.xpath(banners_add_ele.CARREGAR_IMG).should('be.visible');
        cy.xpath(banners_add_ele.DESCRICAO_IMG).should('contain', 'Imagem 1.464x1.044 (JPG ou PNG)')
        cy.xpath(banners_add_ele.GABARITO_IMG).should('contain', 'Ver Gabarito')
        cy.xpath(banners_add_ele.BTN_MODAL('Cancelar')).should('be.visible');
        cy.xpath(banners_add_ele.BTN_MODAL('Adicionar')).should('be.visible');
    }
    static upar_imagem() {
        cy.get(banners_add_ele.UPAR_IMAGEM).attachFile(fixtureImage)
    }
    static fluxo_interno() {
        cy.xpath(banners_add_ele.DESTINO_INTERNO).click()
        cy.xpath(banners_add_ele.DESTINO_INTERNO_OPTIONS('1')).click(); //opções de destino   
        cy.wait(1000)        
        cy.xpath(banners_add_ele.OBJETIVO).click()
        cy.xpath(banners_add_ele.OBJETIVO_OPTIONS('1')).click()
    }
    static selecionar_link(select) {
        cy.xpath(banners_add_ele.LINK_SELECT).click()
        cy.xpath(banners_add_ele.LINK_OPTIONS(select)).click()
    }
    static fluxo_externo(link) {
        cy.xpath(banners_add_ele.DESTINO).type(link)
    }
    static botão_modal(btn_txt) {
        cy.xpath(banners_add_ele.BTN_MODAL(btn_txt), {timeout: 8500}).click();
    }
    static assrtv_modal_final_sucesso() {
        cy.xpath(banners_add_ele.MODAL_FINAL, {timeout: 8500}).should('be.visible')
        cy.xpath(banners_add_ele.MSG_MODAL_SUCESSO, {timeout: 8500}).should('contain', 'Banner adicionado com sucesso!')
    }
    static assrtv_modal_final_desistencia() {
        cy.wait(500)
        cy.xpath(banners_add_ele.MODAL_FINAL, {timeout: 8500}).should('be.visible')
        cy.xpath(banners_add_ele.MSG_MODAL_DESISTENCIA, {timeout: 8500}).should('contain', 'Tem certeza que deseja cancelar o cadastro?')
    }
    static completar_fluxo(btn_txt) {
        cy.xpath(banners_add_ele.BTN_FINAL(btn_txt), {timeout: 8500}).click()
    }
}