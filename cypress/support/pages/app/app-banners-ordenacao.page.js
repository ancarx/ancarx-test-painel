import { banners_ord_ele } from "../../elements/app/app-banners-ordenacao.elements"

export class Banners_ordenar{
    static assertiva_pagina(){
        cy.xpath(banners_ord_ele.TITULO_PAG).should('contain', 'Banners')
        cy.xpath(banners_ord_ele.BTN_ADICIONAR).should('be.visible')
        cy.xpath(banners_ord_ele.BTN_ADICIONAR).then(button =>{
        if(button.is(':disabled')){
            cy.log('O botão está disabled, não é possível cadastrar banner')
        }else{
            cy.log('O botão não está disabled, é possível cadastrar banners')
        }
    })
        cy.xpath(banners_ord_ele.BTN_EDITAR_ORD).should('be.visible')
        cy.xpath(banners_ord_ele.FILTROS_PAG).each(item =>{
            cy.log(item.text())
        })
        cy.xpath(banners_ord_ele.REGISTROS_PAG).should('be.visible')
    }
    static editar_ordenacao(){
        cy.xpath(banners_ord_ele.BTN_EDITAR_ORD).click()
    }
    static ordenar_banners(){
        cy.xpath(banners_ord_ele.POSICAO_SELECT).click()
        cy.xpath(banners_ord_ele.POSICAO_OPTIONS).click()
        cy.xpath(banners_ord_ele.POSICAO_SELECT).click()
        cy.xpath(banners_ord_ele.POSICAO_OPTIONS).click()
    }
    static salvar_ordenação(){
        cy.xpath(banners_ord_ele.SALVAR_ORD).click()
    }
    static cancelar_ordenação(){
        cy.xpath(banners_ord_ele.CANCELAR_ORD).click()
    }
    static assrtv_ordenacao_falha(){
        cy.xpath(banners_ord_ele.MSG_MODAL('Tem certeza que deseja cancelar?')).should('be.visible')
        cy.xpath(banners_ord_ele.BTN_MODAL('Sim, cancelar')).should('be.visible')
    }
    static completar_fluxo_falha(){
        cy.xpath(banners_ord_ele.BTN_MODAL('Sim, cancelar')).click()
    }
    static assrtv_ordenacao_sucesso(){
        cy.xpath(banners_ord_ele.MSG_SUCESSO_ORD).should('contain', 'Edição salva com sucesso!')
        cy.xpath(banners_ord_ele.BTN_SUCESSO_ORD).should('contain', 'Voltar para página de banners')
    }
    static completar_fluxo_sucesso(){
        cy.xpath(banners_ord_ele.BTN_SUCESSO_ORD).click()
    }
    static excluir_banner(){
        cy.xpath(banners_ord_ele.EXCLUIR_BANNER).click()
    }
    static assrtv_exclusão(){
        cy.xpath(banners_ord_ele.MSG_MODAL('Tem certeza que deseja remover esse banner?')).should('be.visible')
        cy.xpath(banners_ord_ele.BTN_MODAL('Sim, quero remover')).should('be.visible')
        
    }
    static completar_exclusão(){
        let API = 'https://api.ancar.com.br/hml/api-ancarx/api/v1/**'
        cy.server()
        cy.route('GET', `${API}`).as('loadBannerExclusion')
        cy.xpath(banners_ord_ele.BTN_MODAL('Sim, quero remover')).click()
        /* cy.wait('@loadBannerExclusion').its('status').should('eq', 200) */
        cy.xpath(banners_ord_ele.VOLTAR_PAGINA).click()
    }
}