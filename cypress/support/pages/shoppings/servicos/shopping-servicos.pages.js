import { shopping_servicos_ele } from '../../../elements/shoppings/servicos/shopping-servicos.elements';

export class Shopping_S{
    static acessar_servicos(){
        cy.xpath(shopping_servicos_ele.NOME_SHOPPING).should('contain', "Shopping Nova America")
        cy.xpath(shopping_servicos_ele.BTN_SERVICOS).should('be.visible').click()
    }
    static assrtv_pag_servicos(){
        cy.xpath(shopping_servicos_ele.NOME_SHOPPING_EM_SERVICOS).should('have.text', 'Serviços (Shopping Nova America)')
        cy.xpath(shopping_servicos_ele.EDITAR_ORDENACAO).should('be.visible')
        cy.get(shopping_servicos_ele.FILTRO_BUSCAR).should('be.visible')
        cy.xpath(shopping_servicos_ele.COLUNA_NOME).should('have.text', 'Nome')
        cy.xpath(shopping_servicos_ele.COLUNA_SIGLA).should('be.visible')
        cy.xpath(shopping_servicos_ele.BTN_ACTIONS).should('be.visible')
    }
    static filtrar_servicos(text){
        cy.get(shopping_servicos_ele.FILTRO_BUSCAR).type(text)
    }
    static assrtv_busca_valida(){
        cy.xpath(shopping_servicos_ele.NOME_SERVICO('1')).then(item =>{
            this.filtrar_servicos(item.text())
            cy.xpath(shopping_servicos_ele.NOME_SERVICO('1')).should('have.text', item.text())
        })
    }
    static assrtv_busca_invalida(){
        cy.xpath(shopping_servicos_ele.IMG_ERROR).should('be.visible')
    }
    static ver_detalhes(){
        const servicos = ['Serviço','Piso do shopping','Complemento','Tipo de URL','URL','Telefone','Email','Descrição (app/site)','Descrição 1 (bot)','Descrição 2 (bot)','Horário de funcionamento']
        cy.xpath(shopping_servicos_ele.BTN_DETALHES).click()

        cy.xpath(shopping_servicos_ele.DETALHES_SERVICOS('','[1]')).each((item, index, list)=>{
            expect(list).to.have.length(11)
            cy.wrap(item).should('contain.text', servicos[index])
        })
    }
    static voltar_breadcrumbs(){
        cy.xpath(shopping_servicos_ele.BREADCRUMBS).click()
        cy.xpath(shopping_servicos_ele.NOME_SHOPPING_EM_SERVICOS).should('have.text', 'Serviços (Shopping Nova America)')
    }
    //DEVE EDITAR ORDENAÇÃO
    static editar_ordenacao(){
        cy.xpath(shopping_servicos_ele.EDITAR_ORDENACAO).should('not.be.disabled').click();
    }
    static drag_and_drop(){
        let nome1 = null;
        let nome2 = null;
        cy.xpath(shopping_servicos_ele.SERVICO('1')).then(($el1) => (nome1 = $el1.text()))
        .then(() =>{
            cy.xpath(shopping_servicos_ele.SERVICO('2')).then(($el2) => (nome2 = $el2.text()))
        .then(() =>{
                cy.xpath(shopping_servicos_ele.SERVICO('1')).dragAndDrop(
                shopping_servicos_ele.SERVICO('1'),
                shopping_servicos_ele.SERVICO('2')
                )
                cy.xpath(shopping_servicos_ele.NOME_SERVICO('1')).should('contain', nome2)
                cy.xpath(shopping_servicos_ele.NOME_SERVICO('2')).should('contain', nome1)
            })
        })
    }
}