import {
    shopping_servicos_alt
} from '../../../elements/shoppings/servicos/shopping-servicos-alterar.elements'

const faker = require('faker')
var loremWords = faker.lorem.words(3)
var loremSentences = faker.lorem.sentence()
export class Shopping_SA {
    static btn_alterar() {
        cy.xpath(shopping_servicos_alt.BTN_ALTERAR).click()
    }
    static assrtv_pagina() {
        cy.interceptAPI().as('loadAssrtv')
        cy.xpath(shopping_servicos_alt.TITULO_PAGINA).should('be.visible')
        cy.wait('@loadAssrtv')
        cy.get(shopping_servicos_alt.PISO_SHOPPING, {timeout: 6500}).should('be.visible')
        cy.get(shopping_servicos_alt.COMPLEMENTO_SHOPPING).should('be.visible')
        cy.get(shopping_servicos_alt.TIPO_URL).should('be.visible')
        cy.get(shopping_servicos_alt.URL).should('be.visible')
        cy.get(shopping_servicos_alt.TELEFONE).should('be.visible')
        cy.get(shopping_servicos_alt.EMAIL).should('be.visible')
        cy.get(shopping_servicos_alt.DESCRICAO_APP).should('be.visible')
        cy.get(shopping_servicos_alt.DESCRICAO_BOT_1).should('be.visible')
        cy.get(shopping_servicos_alt.DESCRICAO_BOT_2).should('be.visible')
        cy.xpath(shopping_servicos_alt.HORARIO_FUNCIONAMENTO_SESSION).click()
        cy.get(shopping_servicos_alt.HORARIO_FUNCIONAMENTO).should('be.visible')
        cy.xpath(shopping_servicos_alt.BTN_PAGE('Salvar edição')).should('be.disabled')
    }
    static preencher_campos() {
        cy.get(shopping_servicos_alt.PISO_SHOPPING).click()
        cy.xpath(shopping_servicos_alt.PISO_SHOPPING_OPTIONS).click()
        cy.get(shopping_servicos_alt.PISO_SHOPPING).click()

        cy.get(shopping_servicos_alt.COMPLEMENTO_SHOPPING).clear().type('AUT-Ao lado da guarita de segurança')
        cy.get(shopping_servicos_alt.TIPO_URL).click()
        cy.xpath(shopping_servicos_alt.TIPO_URL_OPTION).click()
        cy.get(shopping_servicos_alt.URL).clear().type('https://management-panel-dot-ancarx-hom.rj.r.appspot.com/home')
        cy.get(shopping_servicos_alt.TELEFONE).clear().type('11479644444')
        cy.get(shopping_servicos_alt.EMAIL).clear().type('testeautomatizado@test.com')
        cy.get(shopping_servicos_alt.DESCRICAO_APP).clear().type('AUT- Serviço com pagamento automático de estacionamento Sem Parar, ConectCar e Veloe.')
        cy.get(shopping_servicos_alt.DESCRICAO_BOT_1).clear().type(`AUT- ` + `${loremSentences}`)
        cy.get(shopping_servicos_alt.DESCRICAO_BOT_2).clear().type(`AUT- ` + `${loremSentences}`)

        cy.xpath(shopping_servicos_alt.HORARIO_FUNCIONAMENTO_SESSION).click()
        cy.get(shopping_servicos_alt.HORARIO_FUNCIONAMENTO).clear().type('AUT- Seg - Sex: 09:00 ~ 18:00, Sab - Dom: 09:00 ~ 20:00')
    }
    static salvar_alteração() {
        cy.interceptAPI().as('loadSalvar')
        cy.xpath(shopping_servicos_alt.BTN_PAGE('Salvar edição')).should('be.not.disabled').click()
        cy.wait('@loadSalvar')
    }
    static assrtv_modal() {
        cy.xpath(shopping_servicos_alt.MODAL).should('be.visible')
        cy.xpath(shopping_servicos_alt.MODAL_MSG).should('have.text', 'Edição salva com sucesso!')
        cy.xpath(shopping_servicos_alt.MODAL_BTN).should('be.visible')
    }
    static completar_fluxo() {
        cy.xpath(shopping_servicos_alt.MODAL_BTN).click()
    }
}