export class Shopping_Listar_Piso{
    static editar_shopping(){
        cy.xpath('//tr[1]/td[3]//button[3]').click()
    }
    static aba_pisos(){
        cy.wait(1500)
        cy.xpath('//div[@data-testid="modal"]/div[2]/div[2]/div/div//span[contains(.,"Piso")]').click()
    }
    static assrtv_modal(){
        cy.xpath('//div[@data-testid="modal"]/div[2]').should('be.visible')
        cy.xpath('//span[contains(.,"Adicionar piso")]').should('be.visible')
    }
    static criar_piso(){
        cy.xpath('//span[contains(.,"Adicionar piso")]').click()
        cy.get('#name-floor-service').type('AUT - piso')
        cy.xpath('//button[contains(.,"Adicionar")]').click()

        //aceitar modal
        cy.xpath('//div[@data-testid="modal"]/div[2]//button').should('be.visible').click()
    }
    static assrtv_pisos(){
        cy.xpath('//div[@data-testid="modal"]/div[2]//tbody/tr/td[2]//span').each($el =>{
            cy.log($el.text())
        })
    }
}