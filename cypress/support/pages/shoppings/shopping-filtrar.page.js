import { shoppings_ele } from "../../elements/shoppings/shopping-listar.elements"

export class Shopping_F {
    static filtrar(filterString){
        cy.get(shoppings_ele.FILTRO).type(`${filterString}`)
        cy.wait(2000)        
    }
    static resultados_nome(){
            cy.xpath(shoppings_ele.NOME_SHOPPING(''), {timeout: 6000}).each((item) => {
                cy.wrap(item).should('contain.text', 'Shopping');
        })
    }
    static resultados_sigla(){
            cy.xpath(shoppings_ele.SIGLA_SHOPPING(''), {timeout: 6000}).each((item) => {
                cy.wrap(item).should('contain.text', 'SNA');
        })
    }
}