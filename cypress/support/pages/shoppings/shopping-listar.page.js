import { shoppings_ele } from "../../elements/shoppings/shopping-listar.elements"

export class Shopping{
    static assertiva_pag(){
        cy.xpath(shoppings_ele.TITULO_PAG).should('have.text', 'Shoppings')
        cy.get(shoppings_ele.FILTRO).should('be.visible')
        cy.xpath(shoppings_ele.FILTRO_NOME).should('have.text', 'Nome do shopping')
        cy.xpath(shoppings_ele.FILTRO_SIGLA).should('have.text', 'Sigla')
        cy.xpath(shoppings_ele.NOME_SHOPPING('[1]')).should('be.visible')
        cy.xpath(shoppings_ele.SIGLA_SHOPPING('[1]')).should('be.visible')
        cy.xpath(shoppings_ele.SERVICOS_SHOPPING).should('be.visible')
        cy.xpath(shoppings_ele.DETALHES_SHOPPING).should('be.visible')
        cy.xpath(shoppings_ele.EDITAR_SHOPPING).should('be.visible')
    }
}
