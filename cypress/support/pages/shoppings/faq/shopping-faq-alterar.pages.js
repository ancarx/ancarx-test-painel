import { shoppings_faq_alt } from '../../../elements/shoppings/faq/shopping-faq-alterar.elements'

const faker = require ('faker')
var loremWords = faker.lorem.words(3)
var loremSentences = faker.lorem.sentence()
export class Shopping_FAQA{
    static editar_resposta(){
        cy.xpath(shoppings_faq_alt.BTN_EDITAR_RESPOSTA).should('be.not.disabled').click()
    }
    static assrtv_pagina(){
        let msg = 'Acesse o manual tom e voz para responder conforme a Ana Manual tom e voz'
        cy.xpath(shoppings_faq_alt.TITULO_PAG).should('contain', 'Editar resposta')
        cy.get(shoppings_faq_alt.INP_CATEGORIA).should('be.visible')
        cy.get(shoppings_faq_alt.INP_PERGUNTA).should('be.visible')
        cy.get(shoppings_faq_alt.INP_RESPOSTA_SITE).should('be.visible')
        cy.get(shoppings_faq_alt.INP_RESPOSTA_BOT).should('be.visible')
        cy.xpath(shoppings_faq_alt.MSG_MANUAL).should('have.text', msg)
        cy.xpath(shoppings_faq_alt.PAGE_BTN('1')).should('be.visible') //cancelar edição
        cy.xpath(shoppings_faq_alt.PAGE_BTN('2')).should('be.visible') //salvar edição

    }
    static preencher_campos(question, answer, shortanswer){
        cy.wait(1000)
        cy.get(shoppings_faq_alt.INP_CATEGORIA).click()
        cy.xpath(shoppings_faq_alt.INP_CATEGORIA_OPTION).click()
        cy.get(shoppings_faq_alt.INP_PERGUNTA).clear().type(question)
        cy.get(shoppings_faq_alt.INP_RESPOSTA_SITE).clear().type(answer)
        cy.get(shoppings_faq_alt.INP_RESPOSTA_BOT).clear().type(shortanswer)
    }
    static botao_final(length){
        cy.xpath(shoppings_faq_alt.PAGE_BTN(length), {timeout: 8500}).click() //salvar edição
    }
    static assrtv_sucesso(){
        cy.xpath(shoppings_faq_alt.MODAL).should('be.visible')
        cy.xpath(shoppings_faq_alt.MSG_SUCESSO).should('have.text', 'Edição salva com sucesso!')
        cy.xpath(shoppings_faq_alt.BTN_MODAL).should('be.visible')
    }
    static assrtv_falha(){
        cy.xpath(shoppings_faq_alt.MSG_FALHA).should('contain', 'preenchimento obrigatório')
    }
    static assrtv_desistencia(){
        cy.xpath(shoppings_faq_alt.MODAL).should('be.visible')
        cy.xpath(shoppings_faq_alt.MSG_DESISTENCIA).should('contain', 'Tem certeza que deseja cancelar?') //msg de sucesso
        cy.xpath(shoppings_faq_alt.BTN_DESISTENCIA('1')).should('contain', 'Desistir')
        cy.xpath(shoppings_faq_alt.BTN_DESISTENCIA('2')).should('contain', 'Sim, cancelar') //botão
    }
    static completar_desistencia(){
        cy.xpath(shoppings_faq_alt.BTN_DESISTENCIA('2'), {timeout: 8500}).click()
    }
    static completar_fluxo(){
        cy.xpath(shoppings_faq_alt.BTN_MODAL, {timeout: 8500}).click()
    }
}