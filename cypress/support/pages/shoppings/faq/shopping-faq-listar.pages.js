import { shopping_faq_listar } from "../../../elements/shoppings/faq/shopping-faq-listar.elements"

export class Shopping_FAQ{
    static selecionar_faq(){
        cy.xpath(shopping_faq_listar.BTN_FAQ).click()
    }
    static assrtv_pagina(){
        cy.xpath(shopping_faq_listar.TITULO_PAG).should('contain', 'FAQ')
        cy.xpath(shopping_faq_listar.FILTRO_CATEGORIA).should('be.visible').should('have.text', 'Categoria')
        cy.xpath(shopping_faq_listar.FILTRO_PERGUNTA).should('be.visible').should('have.text', 'Pergunta')
        cy.xpath(shopping_faq_listar.FILTRO_RESPOSTA).should('be.visible').should('have.text', 'Resposta')
        cy.xpath(shopping_faq_listar.BTN_ADICIONAR_RESPOSTA).should('be.visible')
    }
    static listar_faq(){
            for(var i = 1; i < 10; i++){
                cy.xpath(shopping_faq_listar.LISTA_PAG(i)).should('be.visible')
            }
    }
}