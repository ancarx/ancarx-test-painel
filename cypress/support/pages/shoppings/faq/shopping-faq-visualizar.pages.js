import { shopping_faq_visu } from "../../../elements/shoppings/faq/shopping-faq-visualizar.elements"

export class Shopping_FAQV {
    static ver_detalhes(){
        cy.xpath(shopping_faq_visu.BTN_VER_DETALHES).click()
    }
    static pega_valores_e_compara_com_detalhe() {
        describe('pega elementos primarios', function () {
            let catFAQ = null
            let perguntaFAQ = null
            let respostaFAQ = null
            cy.xpath(shopping_faq_visu.VALORES_FAQ('1')).then(($el) => (catFAQ = $el.text()))
                .then(() => {
                    cy.xpath(shopping_faq_visu.VALORES_FAQ('2')).then(($el) => (perguntaFAQ = $el.text()))
                        .then(() => {
                            cy.xpath(shopping_faq_visu.VALORES_FAQ('3')).then(($el) => (respostaFAQ = $el.text()))
                                .then(() => {
                                    cy.xpath(shopping_faq_visu.BTN_VER_DETALHES).click()
                                    cy.xpath(shopping_faq_visu.VALORES_DETALHE('1')).should('contain', catFAQ)
                                    cy.xpath(shopping_faq_visu.VALORES_DETALHE('2')).should('contain', perguntaFAQ)
                                    cy.xpath(shopping_faq_visu.VALORES_DETALHE('3')).should('contain', respostaFAQ)
                                    cy.xpath(shopping_faq_visu.VALORES_DETALHE('4')).then(($el) => {
                                        cy.log($el.text())
                                    });
                                })
                        })
                })
        })
    }
    static assrtv_pagina() {
        cy.xpath(shopping_faq_visu.TITULO_PAG).should('contain', 'Ver mais detalhes')
        cy.xpath(shopping_faq_visu.BTN_EDITAR_RESPOSTA).should('be.visible')
        cy.xpath(shopping_faq_visu.FILTRO_DETALHES('1')).should('contain', 'Categoria')
        cy.xpath(shopping_faq_visu.FILTRO_DETALHES('2')).should('contain', 'Pergunta')
        cy.xpath(shopping_faq_visu.FILTRO_DETALHES('3')).should('contain', 'Resposta (site)')
        cy.xpath(shopping_faq_visu.FILTRO_DETALHES('4')).should('contain', 'Resposta (bot)')
    }
}