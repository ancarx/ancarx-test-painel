import { shopping_faq_filtrar } from "../../../elements/shoppings/faq/shopping-faq-filtrar.elements"

export class Shopping_FAQF {
    static filtrar_faq() {
        cy.xpath(shopping_faq_filtrar.BTN_FILTRAR).should('be.not.disabled').click()
    }
    static assrtv_modal() {
        cy.xpath(shopping_faq_filtrar.MODAL).should('be.visible') //modal
        cy.xpath(shopping_faq_filtrar.TITULO_MODAL).should('have.text', 'Filtrar') //titulo
        cy.get(shopping_faq_filtrar.INP_CATEGORIA).should('be.visible')
        cy.xpath(shopping_faq_filtrar.BTN_MODAL('1')).should('be.visible') //limpar filtro
        cy.xpath(shopping_faq_filtrar.BTN_MODAL('2')).should('be.visible') // filtrar
    }
    static limpar_filtro() {
        cy.xpath(shopping_faq_filtrar.BTN_MODAL('1')).click()
    }
    static filtrar() {
        cy.xpath(shopping_faq_filtrar.BTN_MODAL('2')).click()
    }
    static preencher_modal() {
        cy.get(shopping_faq_filtrar.INP_CATEGORIA).click()
        cy.xpath(shopping_faq_filtrar.INP_CATEGORIA_OPTION).click()
    }
    static filtrar_e_fazer_assertiva() {
        let categoria = null
        cy.get(shopping_faq_filtrar.INP_CATEGORIA).click()
        cy.xpath(shopping_faq_filtrar.INP_CATEGORIA_OPTION).then(($el) => (categoria = $el.text()))
            .then(() => {
                cy.xpath(shopping_faq_filtrar.INP_CATEGORIA_OPTION).click()
                cy.xpath(shopping_faq_filtrar.BTN_MODAL('2')).click()
                cy.xpath(shopping_faq_filtrar.ASSRTV_CATEGORIA).each(($el) => {
                    cy.wrap($el).should('have.text', categoria)
                })
            })
    }
    static buscar_faq(){
        cy.get(shopping_faq_filtrar.INP_BUSCAR).type('programação')
    }
    static assrtv_busca(){
        cy.xpath(shopping_faq_filtrar.ASSRTV_CATEGORIA).each(($el1) => {
                    if($el1.text() === 'Programação'){
                        cy.wrap($el1).should('contain', 'Programação')
                    }else{
                        cy.xpath(shopping_faq_filtrar.ASSRTV_PERGUNTA || shopping_faq_filtrar.ASSRTV_RESPOSTA).should('contain', 'programação' || 'Programação')
                    }
                })
    }
}