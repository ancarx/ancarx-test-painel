import {shopping_faq_criar} from '../../../elements/shoppings/faq/shopping-faq-criar.elements'

export class Shopping_FAQC{
    static criar_faq(){
        cy.xpath(shopping_faq_criar.BTN_ADICIONAR_RESPOSTA).should('be.visible').click()
    }
    static assrtv_pag(){
        cy.xpath(shopping_faq_criar.TITULO_PAG).should('have.text', 'Adicionar resposta')
        cy.get(shopping_faq_criar.INP_CATEGORIA).should('be.visible')
        cy.get(shopping_faq_criar.INP_PERGUNTA).should('be.visible')
        cy.get(shopping_faq_criar.INP_RESPOSTA_SITE).should('be.visible')
        cy.get(shopping_faq_criar.INP_RESPOSTA_BOT).should('be.visible')
        cy.xpath(shopping_faq_criar.PAGE_BTN('1')).should('be.visible') //cancelar
        cy.xpath(shopping_faq_criar.PAGE_BTN('2')).should('be.visible') //salvar
    }
    static preencher_campos(){
        cy.get(shopping_faq_criar.INP_CATEGORIA).click()
        cy.xpath(shopping_faq_criar.INP_CATEGORIA_OPTION).click()
        cy.get(shopping_faq_criar.INP_PERGUNTA).type('Qual é a programação para sábado?')
        cy.get(shopping_faq_criar.INP_RESPOSTA_SITE).clear().type('AUT- Todos os sábados e domingos show ao vivo na praça de alimentação')
        cy.get(shopping_faq_criar.INP_RESPOSTA_BOT).clear().type('Sábado e domingo tem programação especial')
    }
    static adicionar_faq(){
        cy.xpath(shopping_faq_criar.BTN_ADICIONAR).click()
    }
    static assrtv_modal(){
        cy.xpath(shopping_faq_criar.MODAL).should('be.visible')
        cy.xpath(shopping_faq_criar.MSG_SUCESSO).should('have.text', 'Adicionado com Sucesso!')
        cy.xpath(shopping_faq_criar.BTN_MODAL).should('be.visible')
    }
    static confirmar_faq(){
        cy.xpath(shopping_faq_criar.BTN_MODAL).click()
    }
}