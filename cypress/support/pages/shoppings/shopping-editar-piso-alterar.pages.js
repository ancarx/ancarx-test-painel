import { shopping_editar_piso_alt_ele } from '../../elements/shoppings/shopping-editar-piso-alterar.elements'

const faker = require ('faker')
var loremWords = faker.lorem.words(1)
export class Shopping_Edit_Piso{
    static btn_editar_piso(){
        cy.xpath(shopping_editar_piso_alt_ele.BTN_EDITAR_PISO).click()
    }
    static preencher_edicao(){
        cy.get(shopping_editar_piso_alt_ele.NOME_PISO).clear().type('AUT - piso'+`${loremWords}`)
    }
    static alterar_edicao(){
        cy.interceptAPI().as('loadPiso')
        cy.xpath(shopping_editar_piso_alt_ele.BTN_ALTERAR).click()
        cy.wait('@loadPiso')
    }
    static assrtv_modal(){
        cy.xpath(shopping_editar_piso_alt_ele.MODAL).should('be.visible')
        cy.xpath(shopping_editar_piso_alt_ele.BTN_MODAL).should('be.visible')
    }
    static terminar_fluxo(){
        cy.xpath(shopping_editar_piso_alt_ele.BTN_MODAL).click()
    }
}