/// <references="Cypress" />
import { eventos_alt_ele } from "../../elements/eventos/eventos-alterar.elements"
import { eventos_filtro_ele } from "../../elements/eventos/eventos-filtrar.elements"
import { eventos_ele } from "../../elements/eventos/eventos-criar.elements"

const faker = require ('faker')
var loremWords3 = faker.lorem.words(3)
var loremWord1 = faker.lorem.word(1)
export class Eventos_A {
    static filtrar_evento(){
        cy.wait(1000)
        cy.xpath(eventos_ele.BTN_FILTRAR).should('not.be.disabled').click()
        cy.xpath(eventos_filtro_ele.ASSRTV_MODAL, {timeout: 6000}).should('be.visible')
        cy.get(eventos_filtro_ele.PERIODO_INICIAL).type('01012029')
        cy.get(eventos_filtro_ele.PERIODO_FINAL).type('02012029')
        cy.xpath(eventos_filtro_ele.BTN_FILTRO).click()
    }
    static editar_evento(){
        cy.xpath(eventos_alt_ele.BTN_EDITAR)
            .should('not.be.disabled')
            .click();
    }
    static salvar_edicao(){
        cy.xpath(eventos_alt_ele.BTN_SALVAR_EDICAO)
            .should('not.be.disabled')
            .click();
    }
    static cancelar_edicao(){
        cy.xpath(eventos_alt_ele.BTN_CANCELAR_EDICAO).click();
    }
    static editar_campos(){
        cy.get(eventos_alt_ele.NOME_EVENTO).clear().type('(NÃO EXCLUIR) AUTOMAÇÃO - '+`${loremWord1}`),
        cy.get(eventos_alt_ele.DESCRICAO_APP).clear().type('Teste Cypress - EDIT'),
        cy.get(eventos_alt_ele.REGULAMENTOS_REGRAS).clear().type('AUTOMAÇÃO - ' +`${loremWords3}` )
    }
    static assertiva_modal_cancelamento(){
        cy.xpath(eventos_alt_ele.MODAL).should('be.visible')
        cy.xpath(eventos_alt_ele.ASSRTV_TEXT_MODAL_DESISTENCIA).should('contain', "Tem certeza que deseja cancelar o cadastro?")
        cy.xpath(eventos_alt_ele.BTN_CONFIRM_CANCELAMENTO).should('be.visible')
        cy.xpath(eventos_alt_ele.BTN_DESISTIR_CANCELAMENTO).should('be.visible')
    }
    static confirmar_cancelamento(){
        cy.xpath(eventos_alt_ele.BTN_CONFIRM_CANCELAMENTO).click()
        cy.xpath(eventos_alt_ele.TITULO_PAGINA).should('be.visible')
    }
    static assertiva_modal_edicao(){
        cy.xpath(eventos_alt_ele.ASSRTV_TEXTO_MODAL, {timeouit: 10000}).should('have.text', 'Edição salva com sucesso!')
        cy.xpath(eventos_alt_ele.BTN_CONFIRM_EDICAO).should('be.visible')
    }
    static confirmar_edicao(){
        var api = 'ms_events_http/events?**'
        cy.intercept(`${api}`).as('loadEvents2')
        cy.xpath(eventos_alt_ele.BTN_CONFIRM_EDICAO).click()
        cy.wait('@loadEvents2')
        cy.xpath(eventos_alt_ele.TITULO_PAGINA).should('be.visible')
    }
}