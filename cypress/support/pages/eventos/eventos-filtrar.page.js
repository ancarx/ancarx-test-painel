import { eventos_filtro_ele } from '../../elements/eventos/eventos-filtrar.elements';
import { eventos_ele } from '../../elements/eventos/eventos-criar.elements';

export class Eventos_F {
//TODO botar o passo de "cancelar" um evento

    static acessar_filtro(){
        cy.xpath(eventos_ele.BTN_FILTRAR).click();
    }
    static visualizar_modal(){
        cy.xpath(eventos_filtro_ele.ASSRTV_MODAL, {timeout: 6000}).should('be.visible')
    }
    static botao_limpar_existe(){
        cy.xpath(eventos_filtro_ele.BTN_LIMPAR_FILTRO).should('be.visible')
    }
    static preencher_filtro(){
        cy.get(eventos_filtro_ele.SITUACAO).click();
        cy.xpath(eventos_filtro_ele.SITUACAO_SELECT('Cancelado')).click() //SELECIONA SITUAÇÃO DE CANCELADO
    }
    static clicar_filtrar(){
        cy.interceptAPI().as('loadEvents')
        cy.xpath(eventos_filtro_ele.BTN_FILTRO).click()
        cy.wait('@loadEvents')
    }
    static fazer_assertiva(){
        cy.xpath(eventos_filtro_ele.ASSRTV_SITUACAO_DOS_EVENTOS).each((item) => {
            cy.wrap(item).should('contain.text', 'Cancelado');
        })
    }
}