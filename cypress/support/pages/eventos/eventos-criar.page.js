import {eventos_ele} from '../../elements/eventos/eventos-criar.elements'
const fixtureImage = 'aj11.jpg'

const dayjs = require('dayjs')
const faker = require ('faker')
var loremWords = faker.lorem.words(4)
var loremSentences = faker.lorem.sentence()
export class Eventos {
    
    static acessar_eventos(){
        cy.xpath(eventos_ele.BTN_ACESSA_EVENTOS).click()
    }
    static assertiva_pag_eventos(){
        cy.xpath(eventos_ele.FILTRO_IMAGEM).should('have.text', 'Imagem');
        cy.xpath(eventos_ele.FILTRO_NOME).should('have.text', 'Nome do Evento');
        cy.xpath(eventos_ele.FILTRO_SITUACAO).should('have.text', 'Situação');
        cy.xpath(eventos_ele.FILTRO_SESSOES).should('have.text', 'Sessões');
        cy.xpath(eventos_ele.FILTRO_INICIO_EVENTO).should('have.text', 'Início do evento');
        cy.xpath(eventos_ele.FILTRO_FIM_EVENTO).should('have.text', 'Final do evento');
        cy.xpath(eventos_ele.BTN_CRIAR_EVENTO).should('be.visible');
        cy.xpath(eventos_ele.BTN_FILTRAR).should('be.visible')
    }
    static acessar_criar_evento(){
        cy.xpath(eventos_ele.BTN_CRIAR_EVENTO).should('not.be.disabled').click()
    }
    static upar_imagem(){
        cy.xpath(eventos_ele.BTN_CARREGAR_IMAGEM, {timeout: 6000}).attachFile(fixtureImage)
    }
    static preenche_campos(){
        cy.get(eventos_ele.NOME_EVENTO).type(`Automação - `+`${loremWords}`),
        cy.xpath(eventos_ele.CATEGORIA).click(),
        cy.xpath(eventos_ele.CATEGORIA_SELECT).click(),
        cy.xpath(eventos_ele.RECOMENDACAO).click(),
        cy.xpath(eventos_ele.RECOMENDACAO_SELECT).click(),
        cy.get(eventos_ele.URL).type('https://management-panel-dot-ancarx-hom.rj.r.appspot.com/home')
        cy.get(eventos_ele.DESCRICAO_APP).type(loremSentences),
        cy.get(eventos_ele.DESCRICAO_BOT).type(loremSentences),
        cy.get(eventos_ele.REGULAMENTOS_REGRAS).type('Evento referente à automação para criação de eventos')
        cy.get(eventos_ele.DATA_INICIAL_EXIB).type(dayjs().format('DD/MM/YYYY')), //Bota a data atual
        cy.get(eventos_ele.HORARIO_INICIAL_EXIB).type(dayjs().add(10, 'minute').format('HH:mm')), //Bota a hora atual + 10 minutos
        cy.get(eventos_ele.DATA_FINAL_EXIB).type(dayjs().add(1, 'day').format('DD/MM/YYYY')), //Bota a data de amanhã (+1 dia da data atual)
        cy.get(eventos_ele.HORARIO_FINAL_EXIB).type(dayjs().add(1, 'hour').format('HH:mm')) //Bota a data atual + 1 hora
    }
    static acessa_modal_evento(){
        cy.xpath(eventos_ele.BTN_ADICIONAR_SESSAO).click(),
        cy.xpath(eventos_ele.MODAL_ADICIONAR_SESSAO).should('be.visible')
    }
    static preenche_modal_com_registro(){
        cy.get(eventos_ele.SESSAO_COM_REGISTRO).click(),
        cy.get(eventos_ele.NOME_DA_SESSAO).type('Aprenda a automatizar em Cypress!'),
        cy.get(eventos_ele.LIMITE_PESSOA).type('10'),
        cy.get(eventos_ele.LIMITE_CAPAC_SESSAO).type('2'),
        cy.get(eventos_ele.INICIO_SESSAO_DATA).type(dayjs().add(1, 'day').format('DD/MM/YYYY')), //Bota a data de amanhã (+1 dia da data atual)
        cy.xpath(eventos_ele.INICIO_SESSAO_HORA).type(dayjs().add(12, 'minute').format('HH:mm')), //Bota a hora atual + 12 minutos
        cy.get(eventos_ele.FIM_SESSAO_DATA).type(dayjs().add(1, 'day').format('DD/MM/YYYY')), //Bota a data de amanhã (+1 dia da data atual)
        cy.xpath(eventos_ele.FIM_SESSAO_HORA).type(dayjs().add(1, 'hour').format('HH:mm')), //Bota a hora atual + 1 hora
        cy.get(eventos_ele.TEMPO_LIMITE).type('1000'),
        cy.xpath(eventos_ele.ADD_SESSAO).click()
    }
    static preenche_modal_sem_registro(){
        cy.get(eventos_ele.SESSAO_SEM_REGISTRO).click(),
        cy.get(eventos_ele.NOME_DA_SESSAO).type('Aprenda a automatizar em Cypress!'),
        cy.get(eventos_ele.INICIO_SESSAO_DATA).type(dayjs().add(1, 'day').format('DD/MM/YYYY')), //Bota a data de amanhã (+1 dia da data atual)
        cy.xpath(eventos_ele.INICIO_SESSAO_HORA).type(dayjs().add(12, 'minute').format('HH:mm')), //Bota a hora atual + 12 minutos
        cy.get(eventos_ele.FIM_SESSAO_DATA).type(dayjs().add(1, 'day').format('DD/MM/YYYY')), //Bota a data de amanhã (+1 dia da data atual)
        cy.xpath(eventos_ele.FIM_SESSAO_HORA).type(dayjs().add(1, 'hour').format('HH:mm')), //Bota a hora atual + 1 hora
        cy.xpath(eventos_ele.ADD_SESSAO).click()
    }
    static assertiva_modal_evento(){
        cy.xpath(eventos_ele.ASSRTV_MODAL_EVENTOS).should('have.text', 'Adicionado com sucesso!'),
        cy.xpath(eventos_ele.BTN_VOLTAR_CRIAR_EVENTO).click()
    }
    static salvar_publicar_evento(){
        cy.interceptAPI().as('loadEvents2')
        cy.xpath(eventos_ele.SALVAR_PUBLICAR).click()
        cy.wait('@loadEvents2')
    }
    static assrtv_cadastro_salvo(){
        cy.xpath(eventos_ele.CONFIRMACAO_CADASTRO_SALVO, {timeout: 10000}).should('be.visible')
    }
    static voltar_para_lista(){
        cy.xpath(eventos_ele.VOLTAR_P_LISTA_EVENTOS).click()
    }
}