/// <references="Cypress" />
import { eventos_detalhes_ele } from "../../elements/eventos/eventos-detalhes.elements";
export class Eventos_D {
    static pega_valores_e_compara_com_detalhe(){
        describe('pega elementos primarios', function(){
            let nameEvent = null
            let situacaoEvent = null
            let inicioEvent = null
            let fimEvent = null
            cy.xpath(eventos_detalhes_ele.NOME_EVENTO).then(($el) => (nameEvent = $el.text()))
            .then(() =>{
                cy.interceptAPI().as('loadDetails')
                cy.xpath(eventos_detalhes_ele.SITUACAO_EVENTO).then(($el) => (situacaoEvent = $el.text()))
                .then(() =>{
                    cy.xpath(eventos_detalhes_ele.INICIO_EVENTO).then(($el) => (inicioEvent = $el.text()))
                    .then(() =>{
                        cy.xpath(eventos_detalhes_ele.FIM_EVENTO).then(($el) => (fimEvent = $el.text()))
                        .then(() =>{
                            cy.xpath(eventos_detalhes_ele.DETALHES(nameEvent)).click()
                            cy.wait('@loadDetails');
                            cy.xpath(eventos_detalhes_ele.THUMB_DETALHE).should('be.visible');
                            cy.xpath(eventos_detalhes_ele.NOME_EVENTO_DETALHE).should('contain', nameEvent);
                            cy.xpath(eventos_detalhes_ele.SITUACAO_EVENTO_DETALHE).should('contain', situacaoEvent);
                            /* cy.xpath(eventos_detalhes_ele.DATA_SESSAO_DETALHE).should('contain', inicioEvent);
                            cy.xpath(eventos_detalhes_ele.DATA_SESSAO_DETALHE).should('contain', fimEvent); */
                        })
                    })
                })
            })
        })
    }
    static voltar_para_lista_de_eventos(){
        cy.xpath(eventos_detalhes_ele.BREADCRUMB_EVENTOS).click()
    }
    //Listar os eventos do shopping SNA
    static assertiva_eventos(){
        cy.xpath(eventos_detalhes_ele.THUMB_EVENTO).should('be.visible');
        cy.xpath(eventos_detalhes_ele.NOME_EVENTO).should('be.visible');
        cy.xpath(eventos_detalhes_ele.SITUACAO_EVENTO).should('be.visible');
        cy.xpath(eventos_detalhes_ele.INICIO_EVENTO).should('be.visible');
        cy.xpath(eventos_detalhes_ele.FIM_EVENTO).should('be.visible');
    }
}