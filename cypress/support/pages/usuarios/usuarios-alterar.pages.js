import { usuarios_alterar_ele } from '../../elements/usuarios/usuarios-alterar.elements';

const faker = require('faker');
let word = faker.lorem.word()
export class Usuarios_A {
    static busca_usuario(){
        cy.xpath(usuarios_alterar_ele.CAMPO_BUSCA, {timeout: 10000}).type('Cypress USER')
    }
    static editar_usuario(){
        cy.xpath(usuarios_alterar_ele.USER).should('be.visible')
        cy.xpath(usuarios_alterar_ele.USER_EDIT).click()
    }
    static editar_dados(){
        cy.get(usuarios_alterar_ele.USER_EDIT_NAME).clear().type(`Cypress USER - `+ `${word}`)
        cy.get(usuarios_alterar_ele.USER_PASSWORD).clear().type('abc456')
        cy.get(usuarios_alterar_ele.USER_CONFIRM_PASSWORD).clear().type('abc456')
    }
    static confirmar_alteracao(){
        cy.xpath(usuarios_alterar_ele.BTN_ALTERAR).should('not.be.disabled').click()
    }
    static confirmacao_modal(){
        cy.xpath(usuarios_alterar_ele.MODAL_CONFIRMACAO).should('be.visible')
        cy.xpath(usuarios_alterar_ele.MSG_CONFIRMACAO)
                                .should('be.visible')
                                .should('contain', 'sucesso')
    }
    static assertiva_nome_editado(){
        cy.visit('/user')
        cy.xpath(usuarios_alterar_ele.CAMPO_BUSCA).type('Cypress USER')
        cy.xpath(usuarios_alterar_ele.USER).should('be.visible')
        cy.xpath(usuarios_alterar_ele.USER).should('contain', `${word}`)
    }
}