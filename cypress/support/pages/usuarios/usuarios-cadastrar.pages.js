import { usuarios_ele } from "../../elements/usuarios/usuarios-cadastrar.elements"

const faker = require('faker');
let username = faker.name.findName()
let email = faker.internet.email()
export class Usuarios {
    static assertiva_pag_usuarios(){
        cy.xpath(usuarios_ele.TITULO_PAGINA).should('have.text', 'Lista de Usuários');
        cy.xpath(usuarios_ele.FILTRO_NOME).should('have.text', 'Nome');
        cy.xpath(usuarios_ele.FILTRO_EMAIL).should('have.text', 'E-mail');
        cy.xpath(usuarios_ele.FILTRO_PERFIL).should('have.text', 'Perfil');
        cy.xpath(usuarios_ele.FILTRO_SHOPPING).should('have.text', 'Shopping');
        cy.xpath(usuarios_ele.FILTRO_SITUACAO).should('have.text', 'Situação');
        cy.xpath(usuarios_ele.BTN_ADICIONAR_USUARIO).should('have.text', 'Adicionar Usuário');
        cy.xpath(usuarios_ele.BTN_FILTRAR).should('have.text', 'Filtrar');
    }
    static adicionar_usuario(){
        cy.xpath(usuarios_ele.BTN_ADICIONAR_USUARIO, {timeout: 6500}).should('be.not.disabled').click();
    }
    static assertiva_modal(){
        cy.xpath(usuarios_ele.MODAL).should('be.visible')
        cy.xpath(usuarios_ele.TITULO_MODAL).should('contain', 'Novo');
        cy.xpath(usuarios_ele.USUARIO_NOME).should('be.visible');
        cy.xpath(usuarios_ele.USUARIO_EMAIL).should('be.visible');
        cy.xpath(usuarios_ele.USUARIO_PERFIL).should('be.visible');
        cy.xpath(usuarios_ele.USUARIO_SHOPPING).should('be.visible');
        cy.xpath(usuarios_ele.BTN_CRIAR).should('be.visible');
    }
    static preencher_modal(perfil){
        cy.xpath(usuarios_ele.USUARIO_NOME).type(`Bot `+`${username}`);
        cy.xpath(usuarios_ele.USUARIO_EMAIL).type(`${email}`);
        cy.xpath(usuarios_ele.USUARIO_PERFIL).click();
        cy.xpath(usuarios_ele.USUARIO_PERFIL_SELECT(perfil)).click();
        cy.xpath(usuarios_ele.USUARIO_SHOPPING).click();
        cy.get(usuarios_ele.USUARIO_SHOPPING_SELECT).click();
    }
    static preencher_modal_lojista(perfil){
        cy.xpath(usuarios_ele.USUARIO_NOME).type(`Bot `+`${username}`);
        cy.xpath(usuarios_ele.USUARIO_EMAIL).type(`${email}`);
        cy.xpath(usuarios_ele.USUARIO_PERFIL).click();
        cy.xpath(usuarios_ele.USUARIO_PERFIL_SELECT(perfil)).click();
        cy.interceptAPI().as('loadShopping')
        cy.xpath(usuarios_ele.USUARIO_SHOPPING).click();
        cy.xpath(usuarios_ele.USUARIO_SHOPPING_SEL_LOJISTA).click();
        cy.wait('@loadShopping')
    }
    static fechar_dropdown(){
        cy.xpath(usuarios_ele.USUARIO_SHOPPING).click(); // Comando executado para que seja desselecionado o Shopping
    }
    static seleciona_loja(){
        cy.get(usuarios_ele.STORE).click()
        cy.xpath(usuarios_ele.STORE_SELECT).click()
    }
    static criar_usuario(){
        cy.xpath(usuarios_ele.BTN_CRIAR).click();
    }
    static assertiva_sucesso(){
        cy.xpath(usuarios_ele.MSG_SUCESSO).should('contain', 'sucesso');
        cy.xpath(usuarios_ele.MODAL_SUCESSO).should('be.visible')
    }
    static preenche_senha(){
        cy.get(usuarios_ele.USER_PASSWORD).type('abc123456')
        cy.get(usuarios_ele.USER_CONFIRM_PASSWORD).type('abc123456')
    }
    //CENÁRIO FALHO
    static preenche_senha_diferente(){
        cy.get(usuarios_ele.USER_PASSWORD).type('ab56ty')
        cy.get(usuarios_ele.USER_CONFIRM_PASSWORD).type('abc123456')
        cy.xpath(usuarios_ele.BTN_CRIAR).click()
        
    }
    static assertiva_senhas_diferentes(){
        cy.xpath(usuarios_ele.SENHAS_DIFERENTES).should('be.visible')
    }
    static assertiva_senha_obrigatoria(){
        cy.xpath(usuarios_ele.PREENCHIMENTO_OBRIGATORIO1).should('have.text', 'preenchimento obrigatório')
        cy.xpath(usuarios_ele.PREENCHIMENTO_OBRIGATORIO2).should('have.text', 'preenchimento obrigatório')
    }
}