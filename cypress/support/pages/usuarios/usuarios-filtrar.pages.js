import { usuarios_filtro_ele } from "../../elements/usuarios/usuarios-filtrar.elements"

export class Usuarios_F{
    static acessar_filtro(){
        cy.xpath(usuarios_filtro_ele.BTN_FILTRAR).should('not.be.disabled').click();
    }
    static assertiva_modal(){
        cy.xpath(usuarios_filtro_ele.MODAL_FILTRO).should('be.visible');
        cy.xpath(usuarios_filtro_ele.TITULO_MODAL).should('be.visible');
        cy.get(usuarios_filtro_ele.PERFIL).should('be.visible');    //ASSERTIVA NO SELECT
        cy.get(usuarios_filtro_ele.SHOPPINGS).should('be.visible'); //ASSERTIVA NO SELECT
        cy.xpath(usuarios_filtro_ele.USUARIOS).should('be.visible');
        cy.xpath(usuarios_filtro_ele.BTN_FILTRAR_MODAL).should('be.visible');
        cy.xpath(usuarios_filtro_ele.BTN_LIMPAR_FILTRO).should('be.visible');
    }
    static preencher_filtro(){
        cy.get(usuarios_filtro_ele.PERFIL).click();
        cy.get(usuarios_filtro_ele.PERFIL_SELECT).click();
        cy.get(usuarios_filtro_ele.PERFIL).click(); //fechar o campo de seleção
        cy.get(usuarios_filtro_ele.SHOPPINGS).click();
        cy.get(usuarios_filtro_ele.SHOPPINGS_SELECT).click();
        cy.get(usuarios_filtro_ele.SHOPPINGS).click(); //fechar o campo de seleção
        cy.get(usuarios_filtro_ele.USUARIOS_SELECT).dblclick();
    }
    static filtrar(){
        cy.interceptAPI().as('loadUsers')
        cy.xpath(usuarios_filtro_ele.BTN_FILTRAR_MODAL).click();
        cy.wait('@loadUsers')
    }
    static assertiva_filtrados_por_situacao(){
         cy.xpath(usuarios_filtro_ele.SITUACAO_FILTRADO).each(item => {
            cy.wrap(item).should('contain.text', 'Ativo');
        })
    }
    static assertiva_filtrados_por_perfil(){
        cy.xpath(usuarios_filtro_ele.PERFIL_FILTRADO).each(item => {
            cy.wrap(item).should('contain.text', 'Super Admin');
        })
    }
}