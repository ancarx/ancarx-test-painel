/// <reference types="Cypress" />
import {promotoria_ele} from '../../elements/promotoria/promotoria-checkin.elements'

export class Promotoria {
    static assertiva_pagina(){
        cy.xpath(promotoria_ele.TITULO_PAG, {timeout: 6000}).should('be.visible')
        cy.xpath(promotoria_ele.THUMB_EVENTO, {timeout: 6000}).should('be.visible')
        cy.xpath(promotoria_ele.TITULO_EVENTO, {timeout: 6000}).should('be.visible')
    }
    static escolher_evento(){
        cy.visit('/event-promoter/c8085de2-f381-4e5f-9f12-1a7cf9020377/event-session') //escolhe o evento "Automação - et"
    }
    static escolhe_sessão(){
        cy.xpath(promotoria_ele.ESCOLHER_SESSAO).click({force: true, multiple: true}) //escolhe a sessão "Festa Neon"
    }
    static assertiva_modal_codigo(){
        cy.xpath(promotoria_ele.MODAL_CODIGO).should('be.visible')
        cy.get(promotoria_ele.CODIGO_EVENTO).should('be.visible')
        cy.xpath(promotoria_ele.BTN_VALIDAR).should('be.visible')
        cy.xpath(promotoria_ele.BTN_CANCELAR).should('be.visible')
    }
    static inserir_codigo(cod){
        cy.get(promotoria_ele.CODIGO_EVENTO).type(cod)
    }
    static validar(){
        cy.xpath(promotoria_ele.BTN_VALIDAR).click()
    }
    static assertiva_modal_ingresso(){
        cy.xpath(promotoria_ele.THUMB_INGRESSO).should('be.visible')//thumb
        cy.xpath(promotoria_ele.NOME_EVENTO_INGRESSO).should('contain', 'Cypress') //nome eevnt
        cy.xpath(promotoria_ele.DADOS_INGRESSO).should('be.visible') // dados eevnto
        cy.xpath(promotoria_ele.TITULAR_INGRESSO).should('be.visible').then(($titular) =>{
            const tit = $titular.text()
            cy.log(tit)//nome titular
    })
    cy.xpath(promotoria_ele.CLASSIFICACAO_INGRESSO).should('be.visible') // classificação indicativa
    cy.xpath(promotoria_ele.ACOMPANHANTES_INGRESSO).each((item) => {
            cy.log(item.text())
        })
        cy.xpath(promotoria_ele.CHECKBOX_ACESSO).should('be.visible')
    }
    static liberar_acesso(){
        cy.xpath(promotoria_ele.BTN_LIBERAR_ACESSO).click()
    }
    static assertiva_modal_confirmação(){
        cy.xpath(promotoria_ele.MODAL_CONFIRMACAO).should('be.visible')
        cy.xpath(promotoria_ele.MSG_CONFIRMACAO).should('contain', 'Acesso liberado!')
        cy.xpath(promotoria_ele.BTN_NOVA_CONSULTA).should('be.visible')
    }
    static nova_consulta(){
        cy.xpath(promotoria_ele.BTN_NOVA_CONSULTA).click()
    }
    static msg_erro(msg){
        cy.xpath(promotoria_ele.MSG_ERRO_CODIGO).should('contain',msg)
    }
}