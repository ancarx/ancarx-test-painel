import {Base} from '../../elements/basics/base.elements'
import {Logar} from '../../elements/login/login.elements'

export class Basic{
    static acessa_hom(){
        cy.visit(Base.AMB_HOM)
    }
    static acessa_dev(){
        cy.visit(Base.AMB_DEV)
    }
    static reload_page(){
        cy.reload();
    };
    static fazLogin_dev(){
        cy.get(Logar.EMAIL).type('giovani.cologni@compasso.com.br');
        cy.get(Logar.SENHA).type('291928');
        cy.get(Logar.BTN_ENTRAR).click();
        cy.get(Logar.MSG_BOAS_VINDAS, {timeout: 10000})
                .should('be.visible')
                .should('contain', 'Olá, ')
    }
    static fazLogin(){
        cy.get(Logar.EMAIL).type(Cypress.env('username'), { log: false });
        cy.get(Logar.SENHA).type(Cypress.env('password'), { log: false });
        cy.get(Logar.BTN_ENTRAR).click();
        cy.get(Logar.MSG_BOAS_VINDAS, {timeout: 15000})
                .should('be.visible')
                .should('have.text', 'Olá, Vitor Admin')
    }
    static selecionar_shopping_sna(){
        cy.xpath(Base.SHOPPING_CODE, {timeout: 10000}).click();
        cy.xpath(Base.SHOPPING_SELECT_SNA).click()
    }
}