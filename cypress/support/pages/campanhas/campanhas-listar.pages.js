import {campanhas_ele} from '../../elements/campanhas/campanhas-listar.elements'

export class Campanhas{
    static assertiva_pag_campanhas(){
        cy.xpath(campanhas_ele.TITULO_PAG).should('be.visible')
        cy.xpath(campanhas_ele.BTN_CRIAR_CAMPANHA).should('be.visible')
        cy.get(campanhas_ele.INP_BUSCAR).should('be.visible')
        cy.xpath(campanhas_ele.BTN_FILTRAR).should('be.visible')
        cy.xpath(campanhas_ele.FILTRO_IMAGEM).should('be.visible')
        cy.xpath(campanhas_ele.FILTRO_CAMPANHA).should('be.visible')
        cy.xpath(campanhas_ele.FILTRO_SITUACAO).should('be.visible')
        cy.xpath(campanhas_ele.FILTRO_DATA_INICIAL).should('be.visible')
        cy.xpath(campanhas_ele.FILTRO_DATA_FINAL).should('be.visible')
        cy.xpath(campanhas_ele.FILTRO_DESTACADO).should('be.visible')
        cy.xpath(campanhas_ele.FILTRO_LIMITE_CUPONS).should('be.visible')
        cy.xpath(campanhas_ele.FILTRO_OFERTAS).should('be.visible')
    }
    static assertiva_registro_campanhas(){
        cy.xpath(campanhas_ele.REG_THUMB).should('be.visible')
        cy.xpath(campanhas_ele.REG_NOME('[1]')).should('be.visible')
        cy.xpath(campanhas_ele.REG_SITUACAO('[1]')).should('be.visible')
        cy.xpath(campanhas_ele.REG_DATA_INICIAL).should('be.visible')
        cy.xpath(campanhas_ele.REG_DATA_FINAL).should('be.visible')
        cy.xpath(campanhas_ele.REG_DESTACADO).should('be.visible')
        cy.xpath(campanhas_ele.REG_LIMITE_CUPONS).should('be.visible')
        cy.xpath(campanhas_ele.REG_OFERTAS_VINCULADAS).should('be.visible')
        cy.xpath(campanhas_ele.BTN_EDITAR('')).should('be.visible')
        cy.xpath(campanhas_ele.BTN_VISUALIZAR).should('be.visible')
    }
}