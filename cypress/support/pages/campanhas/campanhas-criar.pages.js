import { campanhas_criar_ele } from '../../elements/campanhas/campanhas-criar.elements'
import {campanhas_ele} from '../../elements/campanhas/campanhas-listar.elements'

const fixtureImage = 'jpg1.jpg'
const faker = require ('faker')
var loremWords = faker.lorem.words(3)
var loremSentences = faker.lorem.sentence()
export class Campanhas_Criar{
    static criar_campanha(){
        cy.xpath(campanhas_ele.BTN_CRIAR_CAMPANHA).should('not.be.disabled').click()
    }
    static assertiva_pagina_criar(){
        cy.get(campanhas_criar_ele.NOME_CAMPANHA).should('be.visible')
        cy.get(campanhas_criar_ele.DATA_INICIAL).should('be.visible')
        cy.get(campanhas_criar_ele.HORA_INICIAL).should('be.visible')
        cy.get(campanhas_criar_ele.DATA_FINAL).should('be.visible')
        cy.get(campanhas_criar_ele.HORA_FINAL).should('be.visible')
        cy.xpath(campanhas_criar_ele.DESTACAR_OPTIONS).click().should('be.visible')
        cy.get(campanhas_criar_ele.DESCRICAO_APP).should('be.visible')
        cy.xpath(campanhas_criar_ele.DESCRICAO_BOT).should('be.visible')
        cy.xpath(campanhas_criar_ele.REGULAMENTO).should('be.visible')
        cy.xpath(campanhas_criar_ele.LIMITE_CUPONS).scrollIntoView().should('be.visible')
        cy.xpath(campanhas_criar_ele.THUMBNAIL).should('be.visible')
    }
    static preencher_dados(){
        cy.get(campanhas_criar_ele.NOME_CAMPANHA).type(`AUTOMAÇÃO - `+ `${loremWords}`)
        cy.get(campanhas_criar_ele.DATA_INICIAL).type('01012024')
        cy.get(campanhas_criar_ele.HORA_INICIAL).type('0100')
        cy.get(campanhas_criar_ele.DATA_FINAL).type('01012025')
        cy.get(campanhas_criar_ele.HORA_FINAL).type('0100')
        cy.get(campanhas_criar_ele.DESTACAR_SIM).click()
        cy.get(campanhas_criar_ele.DESCRICAO_APP).type(loremSentences)
        cy.xpath(campanhas_criar_ele.DESCRICAO_BOT).type(loremSentences)
        cy.xpath(campanhas_criar_ele.REGULAMENTO).type(loremSentences)
        cy.xpath(campanhas_criar_ele.LIMITE_CUPONS).type('3')
    }
    static upar_imagem(){
        cy.get(campanhas_criar_ele.UPAR_IMAGEM).attachFile(fixtureImage)
    }
    //CENÁRIO DE SUCESSO
    static salvar_publicar(){
        cy.interceptAPI().as('loadCampaigns')
        cy.xpath(campanhas_criar_ele.BTN_CAMPANHA('Salvar e publicar')).should('not.be.disabled').click()
        cy.wait('@loadCampaigns')
    }
    static modal_sucesso(){ 
        cy.xpath(campanhas_criar_ele.MODAL).should('be.visible')
        cy.xpath(campanhas_criar_ele.MODAL_MSG).should('contain', 'sucesso!')
        cy.interceptAPI().as('loadModal')
        cy.xpath(campanhas_criar_ele.BTN_MODAL('Voltar para lista de campanhas')).click()
        cy.wait('@loadModal')
    }

    //CENÁRIO DE FALHA
    static cancelar(){
        cy.xpath(campanhas_criar_ele.BTN_CAMPANHA('Cancelar')).should('not.be.disabled').click()
    }
    static modal_falha(){
        cy.xpath(campanhas_criar_ele.MODAL).should('be.visible')
        cy.xpath(campanhas_criar_ele.MODAL_MSG_FALHA).should('contain', 'cancelar o cadastro?')
        cy.xpath(campanhas_criar_ele.BTN_MODAL('Desistir')).should('be.visible')
        cy.xpath(campanhas_criar_ele.BTN_MODAL('Sim, cancelar')).should('be.visible')
    }
    static confirmar_cancelar(){
        cy.interceptAPI().as('loadCancel')
        cy.xpath(campanhas_criar_ele.BTN_MODAL('Sim, cancelar')).click()
        cy.wait('@loadCancel')
    }
    //Assertiva feita para ver se foi cadastrada a campanha
    static buscar_campanha(){
        cy.get(campanhas_ele.INP_BUSCAR).type(`AUTOMAÇÃO - `+ `${loremWords}`)
        cy.xpath(campanhas_ele.REG_NOME('')).should('contain', `${loremWords}`)
    }
}