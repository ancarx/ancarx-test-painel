import { campanhas_ele } from '../../elements/campanhas/campanhas-listar.elements';
import { campanhas_detalhes_ele } from '../../elements/campanhas/campanhas-detalhes.elements';

export class Campanhas_Detalhes{
    static pega_valores_e_compara_com_detalhe(){
        describe('pega elementos primarios', function(){
            let nameCampaign = null
            let situacaoCampaign = null
            let inicioCampaign = null
            let fimCampaign = null
            cy.xpath(campanhas_ele.REG_NOME('[1]')).then(($el) => (nameCampaign = $el.text()))
            .then(() =>{
                cy.xpath(campanhas_ele.REG_SITUACAO('[1]')).then(($el) => (situacaoCampaign = $el.text()))
                .then(() =>{
                    cy.xpath(campanhas_ele.REG_DATA_INICIAL).then(($el) => (inicioCampaign = $el.text()))
                    .then(() =>{
                        cy.xpath(campanhas_ele.REG_DATA_FINAL).then(($el) => (fimCampaign = $el.text()))
                        .then(() =>{
                        cy.wait(1000);
                        cy.xpath(campanhas_ele.BTN_VISUALIZAR).click();
        /* thumb  */    cy.xpath(campanhas_detalhes_ele.DETALHES_THUMB).should('be.visible');
        /* nome  */     cy.xpath(campanhas_detalhes_ele.DETALHES_DADOS('1')).should('contain', nameCampaign);
        /* situacao  */ cy.xpath(campanhas_detalhes_ele.DETALHES_DADOS('2')).should('contain', situacaoCampaign);
        /* inicioData */cy.xpath(campanhas_detalhes_ele.DETALHES_DADOS('3')).should('contain', inicioCampaign);
        /* fimData  */  cy.xpath(campanhas_detalhes_ele.DETALHES_DADOS('4')).should('contain', fimCampaign);
                        })
                    })
                })
            })
        })
    }
    static assertiva_pag_detalhes(){
        cy.xpath(campanhas_detalhes_ele.DETALHES_THUMB).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('1')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('2')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('3')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('4')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('5')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('6')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('7')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('8')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('9')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_SESSÕES('10')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_DADOS('5')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_DADOS('6')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_DESCRICAO).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_DADOS('8')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_DADOS('9')).should('be.visible');
        cy.xpath(campanhas_detalhes_ele.DETALHES_DADOS('10')).should('be.visible');
    }
}