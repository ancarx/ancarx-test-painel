import { campanhas_ele } from "../../elements/campanhas/campanhas-listar.elements";
import { campanhas_filtrar_ele } from "../../elements/campanhas/campanhas-filtrar.elements";

export class Campanhas_Filtrar{
    static filtrar_campanha(){
        cy.xpath(campanhas_ele.BTN_FILTRAR).should('be.not.disabled').click();
    }
    static assertiva_modal(){
        cy.xpath(campanhas_filtrar_ele.MODAL_FILTRAR).should('be.visible');
        cy.get(campanhas_filtrar_ele.DATA_INICIAL).should('be.visible');
        cy.get(campanhas_filtrar_ele.DATA_FINAL).should('be.visible');
        cy.get(campanhas_filtrar_ele.SITUACAO).should('be.visible');
        cy.get(campanhas_filtrar_ele.EM_DESTAQUE).should('be.visible');
        cy.xpath(campanhas_filtrar_ele.BTN_MODAL('Limpar Filtro')).should('be.visible')
        cy.xpath(campanhas_filtrar_ele.BTN_MODAL('Filtrar')).should('be.visible')
    }
    static filtrar_por_situacao(){
        cy.get(campanhas_filtrar_ele.SITUACAO).click();
        cy.xpath(campanhas_filtrar_ele.SITUACAO_SELECT('Ativo')).click();
        cy.get(campanhas_filtrar_ele.EM_DESTAQUE).click();
        cy.xpath(campanhas_filtrar_ele.EM_DESTAQUE_SELECT('Sim')).click();
        cy.xpath(campanhas_filtrar_ele.BTN_MODAL('Filtrar')).click();
    }
    static assertiva_filtro_campanha(){
        cy.xpath(campanhas_ele.REG_SITUACAO('')).each((item) => {
            cy.wrap(item).should('contain.text', 'Ativo');
        })
    }
}