import { campanhas_criar_ele } from '../../elements/campanhas/campanhas-criar.elements'
import {campanhas_ele} from '../../elements/campanhas/campanhas-listar.elements'

const faker = require ('faker')
var loremWords = faker.lorem.words(3)
export class Campanhas_Alterar{
    static alterar_campanha(){
        cy.wait(500)
        cy.xpath(campanhas_ele.BTN_EDITAR('AUTOMAÇÃO')).click()
    }
    static alterar_nome(){
        cy.wait(1000)
        cy.get(campanhas_criar_ele.NOME_CAMPANHA).clear().type('AUTOMAÇÃO - '+loremWords+' EDIT')
        cy.get(campanhas_criar_ele.DESCRICAO_APP).clear().type('AUTOMAÇÃO - '+loremWords+' EDIT')
        cy.xpath(campanhas_criar_ele.DESCRICAO_BOT).clear().type('AUTOMAÇÃO - '+loremWords+' EDIT')
    }
    static salvar_edicao(){
        var API = 'https://api.ancar.com.br/hml/api-ancarx/api/v1/campaigns/**'
        cy.interceptAPI().as('loadCampaigns')
        cy.xpath(campanhas_criar_ele.BTN_CAMPANHA('Salvar edição')).should('not.be.disabled').click()
        cy.wait('@loadCampaigns')
    }
    static modal_sucesso(){
        cy.xpath(campanhas_criar_ele.MODAL).should('be.visible')
        cy.xpath(campanhas_criar_ele.MODAL_MSG).should('contain', 'Edição salva com sucesso!')
    }
    static voltar_pra_lista(){
        cy.xpath(campanhas_criar_ele.BTN_MODAL('Voltar para lista de campanhas')).click()
    }
    static buscar_campanha(){
        cy.get(campanhas_ele.INP_BUSCAR).type('AUTOMAÇÃO - '+loremWords+' EDIT')
    }
    static assertiva_campanha(){
        cy.xpath(campanhas_ele.REG_NOME('[1]')).should('contain', 'AUTOMAÇÃO - '+loremWords+' EDIT')
    }
}