/// <references="Cypress" />
import { Nf_filtro_ele } from "../../elements/notafiscal/notafiscal-filtrar.elements";
import { Nf_ele } from "../../elements/notafiscal/notafiscal-avaliar.elements";

export class NF_F{
    static acessar_filtro() {
        cy.xpath(Nf_ele.BTN_FILTRAR).should('not.be.disabled').click();
    }
    static visualizar_modal(){
        cy.xpath(Nf_filtro_ele.VISUALIZAR_MODAL).should('be.visible');
        cy.xpath(Nf_filtro_ele.TITULO_MODAL).should('have.text', 'Filtrar');
    }
    static limpar_filtro_existe(){
        cy.wait(200)
        cy.xpath(Nf_filtro_ele.BTN_LIMPAR_FILTRO).should('be.visible')
    }
    static preencher_filtro_status(){
        cy.get(Nf_filtro_ele.STATUS).click();
        cy.xpath(Nf_filtro_ele.STATUS_SELECT).click();
    }
    static preencher_filtro_shopping(){
        cy.get(Nf_filtro_ele.SHOPPING).click();
        cy.xpath(Nf_filtro_ele.SHOPPING_SELECT).click();
    }
    static clicar_filtrar(){
        cy.xpath(Nf_filtro_ele.BTN_FILTRAR_MODAL).click();
    }
    static faz_assertiva_status(){
        cy.xpath(Nf_filtro_ele.ASSRTV_NOTA_STATUS).each((item) => {
            cy.wrap(item).should('contain.text', 'Reprovada');
        })
    }
    static faz_assertiva_shopping(){
        cy.xpath(Nf_filtro_ele.ASSRTV_NOTA_SHOPPING).each((item) => {
            cy.wrap(item).should('contain.text', 'SNA');
        })
    }
}