/// <references="Cypress" />
import {Nf_ele} from "../../elements/notafiscal/notafiscal-avaliar.elements";

export class NF {
    static assertivas_pagina() {
        cy.xpath(Nf_ele.ASSRTV_TITULO).should('have.text', 'Nota Fiscal');
        cy.xpath(Nf_ele.BTN_AVALIAR_NOTA).should('be.visible');
        cy.xpath(Nf_ele.BTN_FILTRAR).should('be.exist')
    }
    static acessar_avaliacao() {    
        cy.xpath(Nf_ele.BTN_AVALIAR_NOTA, {timeout: 8500}).should('not.be.disabled').click();
        cy.wait(2500) //TODO tirar wait
    }
    static preencher_avaliacao(btn) {
        cy.xpath('//div[@class="container"]//div[@data-testid="card"]/div[2]').then(($body) => {
            if (!$body.find('#cnpj').is(':visible')) {
                cy.xpath(Nf_ele.NOT_FOUND).should('be.visible')
                cy.log('Não há notas no momento!')
            } else {
                this.assert_pagina_avaliacao()
                this.preencher_campos()
                this.btn_acao(btn)
            }
        })
        //1 - com nota disponível caiu no else
        //2 - sem nota disponível caiu no if
    }
    static preencher_reprovacao(btn) {
        cy.xpath('//div[@class="container"]//div[@data-testid="card"]/div[2]').then(($body) => {
            if (!$body.find('#cnpj').is(':visible')) {
                cy.xpath(Nf_ele.NOT_FOUND).should('be.visible')
                cy.log('Não há notas no momento!')
            } else {
                this.assert_pagina_avaliacao()
                this.motivo_reprovacao()
                this.btn_acao(btn)
            }
        }) //1 - com nota disponível caiu no else
        //2 - sem nota disponível caiu no if
    }
    static preencher_adiamento(btn) {
        cy.xpath('//div[@class="container"]//div[@data-testid="card"]/div[2]').then(($body) => {
            if (!$body.find('#cnpj').is(':visible')) {
                cy.xpath(Nf_ele.NOT_FOUND).should('be.visible')
                cy.log('Não há notas no momento!')
            } else {
                this.assert_pagina_avaliacao()
                this.adiar_avaliacao()
                this.btn_acao(btn)
                this.modal_adiamento_assrtv()
                this.modal_adiamento_confirmação()
            }
        }) //1 - com nota disponível caiu no else
        //2 - sem nota disponível caiu no if
    }
    static assert_pagina_avaliacao() {
        const reprov = ['Nota rasurada ou fora de foco', 'Documento não válido', 'CNPJ ilegível', 'Razão social ilegível', 'Número da NF ilegível', 'Valor ilegível', 'Data da compra ilegível', 'O CNPJ da nota não é válido para a promoção', 'Comprovante não aparece na foto', 'QRCode com problemas. Envie a foto da nota', 'Medicamentos não participam da promoção', 'Bebida alcoólica não participa da promoção', 'Enviar notas separadamente', 'Por favor, dirija-se ao Posto de Trocas', 'CPF diferente do cadastrado', 'Outros']
        const adiar = ['10 minutos', '20 minutos', '24 horas']
        const pag = ['Pix', 'Dinheiro', 'Débito', 'Crédito']
        cy.get(Nf_ele.CNPJ).should('be.visible')
        cy.get(Nf_ele.VALOR).should('be.visible');
        cy.get(Nf_ele.NF).should('be.visible');
        cy.get(Nf_ele.FORMA_PAGAMENTO).should('be.visible').click();
        cy.xpath(Nf_ele.FORMA_PAGAMENTO_OPTIONS('')).each((item, index, list) => {
            expect(list).to.have.length(4)
            cy.wrap(item).should('contain.text', pag[index])
        })
        cy.xpath(Nf_ele.BTN('Finalizar avaliação')).should('be.visible')
        cy.xpath(Nf_ele.BTN('Aprovar')).should('be.disabled')
        cy.xpath(Nf_ele.BTN('Reprovar')).should('be.disabled')
        cy.xpath(Nf_ele.BTN('Adiar')).should('be.disabled')
        cy.get(Nf_ele.MOTIVO_REPROVACAO).should('be.visible').click()
        cy.xpath(Nf_ele.MOTIVO_REPROVACAO_OPTIONS('')).each((item, index, list) => {
            expect(list).to.have.length(16)
            cy.wrap(item).should('contain.text', reprov[index])
        })
        cy.get(Nf_ele.TEMPO_ADIAR).should('be.visible').click()
        cy.xpath(Nf_ele.TEMPO_ADIAR_OPTIONS('')).each((item, index, list) => {
            expect(list).to.have.length(3)
            cy.wrap(item).should('contain.text', adiar[index])
        })
    }
    static preencher_campos() {
        cy.get(Nf_ele.LOJA).click()
        cy.xpath(Nf_ele.LOJA_SELECT).click();
        cy.xpath(Nf_ele.DATA_COMPRA_HOJE).click();
        cy.get(Nf_ele.VALOR).type('1222');
        cy.get(Nf_ele.NF).type('55555555555');
        cy.get(Nf_ele.FORMA_PAGAMENTO).click();
        cy.xpath(Nf_ele.FORMA_PAGAMENTO_OPTIONS('[1]')).click()
        cy.get(Nf_ele.CHECKBOX_APROV_PARCIAL).click()
        cy.get(Nf_ele.CHECKBOX_APROV_PARCIAL).click()
    }
    static motivo_reprovacao() {
        cy.get(Nf_ele.MOTIVO_REPROVACAO).click()
        cy.xpath(Nf_ele.MOTIVO_REPROVACAO_OPTIONS('[1]')).click()
    }
    static adiar_avaliacao() {
        cy.xpath(Nf_ele.TEMPO_ADIAR_OPTIONS('[1]')).click()
    }
    static modal_adiamento_assrtv() {
        cy.xpath(Nf_ele.MODAL).should('be.visible')
        cy.xpath(Nf_ele.MODAL_MSG).should('have.text', 'Tem certeza que deseja adiar ?')
        cy.xpath(Nf_ele.MODAL_BTN('1')).should('be.visible')
        cy.xpath(Nf_ele.MODAL_BTN('2')).should('be.visible')
    }
    static modal_adiamento_confirmação() {
        cy.xpath(Nf_ele.MODAL_BTN('2')).click()
    }
    static btn_acao(btn) { //usado para Aprovar, Reprovar, Adiar e Finalizar Avaliação
        cy.xpath(Nf_ele.BTN(btn)).click();
        cy.wait(1000)
    }
}