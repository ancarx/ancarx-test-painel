import {Logar} from '../../elements/login/login.elements'

export class Login{
    static preenche_login(){
        cy.get(Logar.EMAIL).type('vitoradmin@gmail.com', {log: false});
        cy.get(Logar.SENHA).type('12345678', {log: false})
    }
    static preenche_login_falho(){
        cy.get(Logar.EMAIL).type('vitoradmin@gmail.com');
        cy.get(Logar.SENHA).type('12345')
    }
    static visualizar_senha(){
        cy.xpath(Logar.MOSTRAR_SENHA).click();
        cy.get(Logar.SENHA).should('have.value', '12345',)
        cy.xpath(Logar.MOSTRAR_SENHA).click();
    }
    static clica_entrar(){
        cy.xpath(Logar.BTN_ENTRAR).click();
    }
    static assertiva_login_sucess(){
        /* cy.server()
        cy.route('GET', 'https://api.ancar.com.br/hml/api-ancarx/api/v1/panel/authentication/me').as('loadLogin')
        cy.wait('@loadLogin').its('status').should('eq', 200) */
        cy.get(Logar.MSG_BOAS_VINDAS, {timeout: 10000})
                .should('be.visible')
                .should('have.text', 'Olá, Vitor Admin')
    }
    static assertiva_login_falho(){
        cy.xpath(Logar.MSG_ERRO)
                .should('be.visible')
                .should('have.text', 'Esta combinação de e-mail e senha é inválida.')
    }
}